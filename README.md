# stis-engine
# a guile fibers communication layer

Copyright : Stefan Israelsson Tampe
License   : LGPL v2.0 or later


Depends on:
1. libzlib       : zlib compression library
2. libzmq        : Zero MQ message queue library
3. stis-data     : guile scmeme library for serializing data
4. guile-persist : enables fast c-based serialisations
5. fibers        : wingos execllent fibers distribution

Contains:
1. PIPELINING TOOL WITH BUFFERED FIBERS PIPES AKA FPIPES
   a. Basic framework can be found in
   
        (fibers stis-engine fpipes)
      
   b. zlib comperssor fiber pipelines cam be found in
   
        (fibers stis-engine zlib zlib)

   c. a simple and safe scm serializer and decerializer to and from
      fiber pipelines can be found in
      
        (fibers stis-engine atom)

   d. zmq message fiber piplene can be found in

        (fibers stis-engine zmq)

Examples:

Ex1:
#:use-module (fibers)
#
#:use-module (fibers stis-engine fpipes zmq)
#:use-module (fibers stis-engine zmq    zmq)	
#:use-module (fibers stis-engine fpipes fpipes)

(define (test-zmq-fpipe)
  (run-fibers
   (lambda ()
     (define context (zmq-init))
     
     (define socket-client (zmq-socket context ZMQ_REQ))     
     (define socket-server (zmq-socket context ZMQ_REP))

     (zmq-bind    socket-server "tcp://*:5555")
     ;;(zmq-bind socket-server "inproc://a")
     
     (zmq-connect socket-client "tcp://localhost:5555")
     ;;(zmq-connect socket-client "inproc://a")
     (define-values (ch1 ch2)
       (fpipe-construct scm->fpipe
			(fpipe->zmq socket-client)
			(zmq->fpipe socket-server)
			#:pace
			(fpipe->zmq socket-server)
			(zmq->fpipe socket-client)
			fpipe->scm))
     
     (define schemer (fpipe-schemer ch1 ch2))
     (pk (schemer '(alla balla)))
     (pk (length (schemer (iota 1000000)))))))

Ex2:
#:use-module (fibers)
#:use-module (fibers stis-engine fpipes fpipes)
#:use-module (fibers stis-engine fpipes atom)

(define (test-atom-fpipe)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2) (fpipe-construct (atom->fpipe)
					       #:id #:id #:id
					       (fpipe->atom)))
          
     (define scm-schemer  (fpipe-schemer ch1 ch2))

     (pk 'scm-schemer-1  (scm-schemer  '((a b) 2 3)))
     (pk 'scm-schemer-2  (length (scm-schemer (iota 1000000)))))))

Ex3:
#:use-module (fibers)
#:use-module (fibers stis-engine fpipes fpipes)
#:use-module (fibers stis-engine fpipes var)

(define (test-var-fpipe)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2) (fpipe-construct (var->fpipe)
					       #:id #:id #:id
					       (fpipe->var)))
     
     (define scm-schemer  (fpipe-schemer ch1 ch2))
     (define v            (list 1 1 1 1))
     (set-car! v v)
     
     (pk 'scm-schemer-1  (scm-schemer  '((a b) 2 3)))
     (pk 'scm-schemer-2  (scm-schemer v))
     (pk 'scm-schemer-3  (length (scm-schemer (iota 1000000)))))))

Ex4:
#:use-module (fibers)
#:use-module (fibers stis-engine fpipes zmq)
#:use-module (fibers stis-engine fpipes zlib)

(define (test-zlib)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2)
       (fpipe-construct scm->fpipe
			(compress-from-fpipe-to-fpipe)
			(uncompress-from-fpipe-to-fpipe)
			fpipe->scm))

     (define-values (ch3 ch4)
       (fpipe-construct atom->fpipe
			(compress-from-fpipe-to-fpipe)
			(uncompress-from-fpipe-to-fpipe)
			fpipe->atom))

     
     (define compress-schemer1 (fpipe-schemer ch1 ch2))
     (define compress-schemer2 (fpipe-schemer ch3 ch4))


     (define v                 (iota 1000000))
     
     (pk 'compress/uncompress-1 (compress-schemer1 '(1 2 3 4)))
     (pk 'compress/uncompress-2 (compress-schemer2 '(4 3 2 1)))
     (pk 'len (length (compress-schemer2 v))))))



