;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine zmq zmq)
  #:use-module (fibers stis-engine zmq errno)
  #:use-module (oop goops)
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)  
  #:export (zmq-version
            zmq-init
            zmq-term
	    zmq-recv-more?	
	    zmq-msg-resize
	    zmq-msg-cleared?
	    zmq-msg-clear
	    zmq-msg-buf
	    
            zmq-socket
            zmq-close
            ZMQ_PAIR
            ZMQ_PUB
            ZMQ_SUB
            ZMQ_REQ
            ZMQ_REP
            ZMQ_XREQ
            ZMQ_XREP
            ZMQ_PULL
            ZMQ_PUSH
            ZMQ_XPUB
            ZMQ_XSUB
            ZMQ_UPSTREAM
            ZMQ_DOWNSTREAM

            zmq-bind
            zmq-connect

	    make-zmq-msg
	    
            zmq-setsockopt
            zmq-getsockopt
            ZMQ_HWM
            ZMQ_SWAP
            ZMQ_AFFINITY
            ZMQ_IDENTITY
            ZMQ_SUBSCRIBE
            ZMQ_UNSUBSCRIBE
            ZMQ_RATE
            ZMQ_RECOVERY_IVL
            ZMQ_MCAST_LOOP
            ZMQ_SNDBUF
            ZMQ_RCVBUF
            ZMQ_RCVMORE
            ZMQ_FD
            ZMQ_EVENTS
            ZMQ_TYPE
            ZMQ_LINGER
            ZMQ_RECONNECT_IVL
            ZMQ_BACKLOG
            ZMQ_RECOVERY_IVL_MSEC
            ZMQ_RECONNECT_IVL_MAX

            zmq-recv
	    zmq-msg-recv
            zmq-send
	    zmq-msg-send
            ZMQ_DONTWAIT
            ZMQ_SNDMORE))

(define-class <zmq-context> () ctx)
(define-class <zmq-socket>  () ctx socket)

(define (make-context ctx)
  (let ((ret (make <zmq-context>)))
    (slot-set! ret 'ctx ctx)
    ret))

(define (make-socket socket ctx)
  (let ((ret (make <zmq-socket>)))
    (slot-set! ret 'ctx ctx)
    (slot-set! ret 'socket socket)
    ret))

(define ZMQ_PAIR 0)
(define ZMQ_PUB  1)
(define ZMQ_SUB  2)
(define ZMQ_REQ  3)
(define ZMQ_REP  4)
(define ZMQ_XREQ 5)
(define ZMQ_XREP 6)
(define ZMQ_PULL 7)
(define ZMQ_PUSH 8)
(define ZMQ_XPUB 9)
(define ZMQ_XSUB 10)

(define ZMQ_AFFINITY      4)
(define ZMQ_IDENTITY      5)
(define ZMQ_SUBSCRIBE     6)
(define ZMQ_UNSUBSCRIBE   7)
(define ZMQ_RATE          8)
(define ZMQ_RECOVERY_IVL  9)
(define ZMQ_SNDBUF        10)
(define ZMQ_RCVBUF        11)
(define ZMQ_RCVMORE       13)
(define ZMQ_FD            14)
(define ZMQ_EVENTS        15)
(define ZMQ_TYPE          16)
(define ZMQ_LINGER        17)
(define ZMQ_RECONNECT_IVL 18)
(define ZMQ_BACKLOG       19)
(define ZMQ_RECONNECT_IVL_MAX 21)
    
(define ZMQ_DONTWAIT 1)
(define ZMQ_SNDMORE  2)

(define libzmq (dynamic-link "libzmq"))

(define-syntax-rule (define-foreign name ret string-name args)
  (define name
    (pointer->procedure ret (dynamic-func string-name libzmq) args)))

(define-foreign zmq_strerror      '*   "zmq_strerror"      (list int))
(define (zmq-strerror er)
  (pointer->string (zmq_strerror er)))

(define-foreign zmq_version       void "zmq_version"       (list '* '* '*))
(define (zmq-version)
  "Retrieves the version of the zmq library."
  (let ((va (make-bytevector 4))
	(vb (make-bytevector 4))
	(vc (make-bytevector 4)))
    (zmq_version (bytevector->pointer va)
		 (bytevector->pointer vb)
		 (bytevector->pointer vc))
    (list (bytevector-u32-ref va 0 (native-endianness))
	  (bytevector-u32-ref vb 0 (native-endianness))
	  (bytevector-u32-ref vc 0 (native-endianness)))))

(define-foreign zmq_close         int "zmq_close"         (list ' *))
(define (zmq-close socket)
  "Close @var{socket}."

  (when (not (= (zmq_close (slot-ref socket 'socket)) 0))
    (error "failed to cloes ZeroMQ socket")))
  
(define-foreign zmq_init          '*   "zmq_init"          (list int))

(define (null? pt) (= (pointer-address pt) 0))


(define-foreign zmq-msg-init-data% int "zmq_msg_init_data"
  (list '* '* int '* '*))

(define NULL    (make-pointer 0))

(define (zmq-msg-init-data msg buf n)
  (zmq-msg-init-data% msg buf n NULL NULL))
  
(define* (zmq-init #:optional (io-threads 1))
  "Create a ZeroMQ context with optional @var{io-threads} that defaults to 1 threads."
  (let ((pt (zmq_init io-threads)))
    (if (null? pt)
	(error "was not able to initiate a context")
	(make-context pt))))
  
(define-foreign zmq_term          int  "zmq_term"          (list '*))

(define (zmq-term context)
  "Terminate the ZeroMQ context @var{context}."

  (when (not (= 0 (zmq_term (slot-ref context 'ctx))))
    (error "could not terminate context")))


(define-foreign zmq_socket        '*   "zmq_socket"        (list '* int))


(define (zmq-socket context type)
  "Create a new ZeroMQ of type @var{type} within context @var{context}."
  (let ((pt (zmq_socket (slot-ref context 'ctx) type)))
    (when (null? pt)
      (error "could not create a ZeroMQ socket"))
    
    (let ((sock (make-socket pt (slot-ref context 'ctx))))
      sock)))

(define-foreign zmq_setsockopt    int  "zmq_setsockopt"    (list '* int '* int))
(define (%zmq-setsockopt socket opt val)
  "Set @var{opt} to @var{val} on @var{socket}."
  (when (not (= 0 (zmq_setsockopt (slot-ref socket 'socket)
				  opt
				  (bytevector->pointer val)
				  (bytevector-length val))))
    (error (format #f "could not set ZeroMQ socketopt ~a" opt))))

(define-foreign zmq_getsockopt    int  "zmq_getsockopt"    (list '* int '* '*))

(define (%zmq-getsockopt socket opt val)
  "Get @var{opt} of @var{socket} into @var{val}."

  (let* ((n  (bytevector-length val))
	 (v  (make-bytevector 8))
	 (p1 (bytevector->pointer val))
	 (p2 (bytevector->pointer v)))

    (bytevector-u32-set! v 0 n (native-endianness))
    (when (not (= 0 (zmq_getsockopt (slot-ref socket 'socket)
				    opt
				    p1
				    p2)))
      (error (format #f "could not get ZeroMQ socketopt ~a" opt)))

    (when (not (= n (bytevector-u32-ref v 0 (native-endianness))))
      (error (format #f "bad length of socket option: ~a" opt)))))

(define (zmq-recv-more? socket)
  (call-with-blocked-asyncs
   (lambda ()
     (let ((v (make-bytevector 4)))
       (%zmq-getsockopt socket ZMQ_RCVMORE v)
       (= 1 (bytevector-u32-ref v 0 (native-endianness)))))))

(define-foreign zmq_bind          int  "zmq_bind"          (list '* '*))
(define (zmq-bind socket addr)
  "Bind @var{socket} to @var{addr}."

  (zmq_bind (slot-ref socket 'socket) (string->pointer addr)))
  
(define-foreign zmq_connect       int  "zmq_connect"       (list '* '*))
(define (zmq-connect socket addr)
  "Connect to @var{addr} on @var{socket}."

  (zmq_connect (slot-ref socket 'socket) (string->pointer addr)))

(define-foreign zmq-errno         int  "zmq_errno"   (list))

(define-foreign zmq_msg_init_size int  "zmq_msg_init_size" (list '* int))
(define-foreign zmq_msg_data      '*   "zmq_msg_data"      (list '*))
(define-foreign zmq_msg_send      int  "zmq_msg_send"      (list '* '* int))
(define-foreign zmq_msg_close     void "zmq_msg_close"     (list '*))
(define-foreign zmq_send          int  "zmq_send"         (list '* '* long int))

(define* (zmq-msg-send socket msg #:optional (flags 0))
  "Send @var{bv} over ZeroMQ @var{socket} using @var{flags}."
  (call-with-blocked-asyncs
   (lambda ()
     (if (= -1 (zmq_msg_send (car msg) (slot-ref socket 'socket)
			     flags))
	 (errno)
	 #f))))

(define* (zmq-send socket bv n #:optional (flags 0))
  "Send @var{bv} over ZeroMQ @var{socket} using @var{flags}."
  (call-with-blocked-asyncs
   (lambda ()
     (let ((n (zmq_send (slot-ref socket 'socket)
			(bytevector->pointer bv)
			n
			flags)))
       (if (= n -1)
	   (values #f (errno))
	   (values #t n))))))

(define-foreign zmq_msg_recv      int  "zmq_msg_recv"   (list '* '* int))
(define-foreign zmq_recv          int  "zmq_recv"       (list '* '* long  int))
(define-foreign zmq_msg_init      int  "zmq_msg_init"   (list '*))
(define-foreign zmq_msg_size      int  "zmq_msg_size"   (list '*))


(define* (zmq-msg-recv msg socket #:optional (flags 0))
  "Receive a message from @var{socket} with optional flags @var{flags}."

  (call-with-blocked-asyncs
   (lambda ()
     (let ((n (zmq_msg_recv (car msg) (slot-ref socket 'socket)
			    flags)))
       (if (= n -1)
	 (values #f (errno))
	 (values #t n))))))

(define* (zmq-recv socket bv #:optional (flags 0))
  "Receive a message from @var{socket} with optional flags @var{flags}."

  (call-with-blocked-asyncs
   (lambda ()
     (let ((n (zmq_recv (slot-ref socket 'socket)
			(bytevector->pointer bv)
			(bytevector-length bv)
			flags)))
       (if (= n -1)
	 (values #f (errno))
	 (values #t n))))))

(define (make-zmq-msg bufsize)
  (let* ((bv  (make-bytevector 128))
	 (snd (bytevector->pointer bv))
	 (n   (+ bufsize 1))
	 (buf (make-bytevector n)))
    (zmq-msg-init-data snd (bytevector->pointer buf) n)
    (bytevector-u8-set! buf bufsize 1)
    (cons* snd buf bufsize)))

(define (zmq-msg-resize msg bufsize)
  (let* ((bv  (cadr msg))
	 (snd (car  msg))
	 (N   (bytevector-length bv))
	 (n   (+ bufsize 1)))
    (when (> n N)
      (error "Wrong size of zmq msg"))
    
    (zmq-msg-init-data snd (bytevector->pointer bv) n)
    (bytevector-u8-set! bv bufsize 1)
    (set-cdr! (cdr msg) bufsize)))

(define (zmq-msg-buf msg) (cadr msg))
(define (zmq-msg-cleared? msg)
  (let* ((bv  (cadr msg))
	 (n   (cddr msg)))
    (= 0 (bytevector-u8-ref bv (- n 1)))))

(define (zmq-msg-clear msg)
  (let* ((bv  (cadr msg))
	 (n   (cddr msg)))
    (bytevector-u8-set! bv (- n 1) 0)))
  



