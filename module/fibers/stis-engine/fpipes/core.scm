;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes core)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (oop goops)
  #:use-module (srfi srfi-9)
  #:use-module ((rnrs io ports) #:select (eof-object))
  #:use-module (rnrs bytevectors)
  #:export-syntax (Nout-ref
		   Nout-set!
		   mout-ref
		   min-ref
		   nout-ref
		   nout-set!
		   vout-ref)
  #:replace (bytevector-copy!)
  
  #:export (aif
	    id
	    
	    make-fpipe
	    fpipe?
	    
	    ;; Low level
	    fpipe-control?

	    fpipe-eof?
	    fpipe-flush?
	    fpipe-reset?
	    fpipe-end?
	    
	    fpipe-error
	    fpipe-error?
	    fpipe-error-type
	    fpipe-error-msg

	    %fpipe-flush%
	    %fpipe-end%
	    %fpipe-eof%
	    %fpipe-reset%

	    fpipe-swap
	    fpipe-get-u8
	    fpipe-get-bytevector
	    fpipe-peek-u8
	    fpipe-put-u8
	    fpipe-put-bytevector

	    fpipe-regenerate
	    
	    fpipe-handle-control
	    fpipe-schemer

	    ;; Tree helpers
	    tree-map
	    tree-for-each
	    tree-fold
	    tree-or
	    tree-and
	    
	    generate-node
	    ))

#|
The essential protocol

This is a tooöbox for message pipes using buffered pipes or channels that is
efficient for fiber enabled code e.g. code that does not build-up the stack
because the stack is stored and restored when fibers green threads are 
switching context

We have the following control structures
========================================

%fpipe-eof% or eof-object:

    Marks the end of a byte stream representing a message, 
    need to be avoided on channels

%fpipe-flush%

    Will flush the whole pipline from any buffers, need to be transported on
    all kinds of pipes

struct fpipe-error kind:string  error-msg:string

    This Will put a node in error state and send it to downstream
    This should be transported on all error streams

%fpipe-end%

    This will end all threads and be transported on all pipelines

%fpipe-reset%

    This will clear all data and enable the pipline for a new message
    Needs to be transported on all pipelines. Typically when a pipe
    Have errored, the this well be sent to restore the pipeline
  
    A network pipe should work the same way as a channel in order for the 
    client to be able to intereact correctly.

|#

(define-syntax aif
  (syntax-rules ()
    ((_ (it . l) p a b)
     (call-with-values (lambda () p)
       (lambda (it . l)
	 (if it a b))))
    
    ((_ it p a b)
     (let ((it p))
       (if it a b)))))

#|
Internally filled in buffers are sent to the outpt side via mout
and has the possible values

1. eof-object
2. <fpipe-error> object
3. <fpipe-flush> object
4. (pos . buffer)

The put side will first read a message in min and then put the vector on mout
The get side will do things in revers, this way we can peek correctly
|#


(define b-copy! (@ (rnrs bytevectors) bytevector-copy!))

(define (bytevector-copy! source i destination j n)
  (let lp ((n n) (i i) (j j))
    (when (> n 0)
      (let ((k (min n (ash 1 10))))
	(b-copy! source i destination j k)
	(lp (- n k) (+ i k) (+ j k))))))

(define-record-type <fpipe-flush>
  (make-fpipe-flush)
  fpipe-flush?)

(define-record-type <fpipe-end>
  (make-fpipe-end)
  fpipe-end?)

(define-record-type <fpipe-reset>
  (make-fpipe-reset)
  fpipe-reset?)

(define fpipe-eof? eof-object?)

(define %fpipe-reset% (make-fpipe-reset))
(define %fpipe-flush% (make-fpipe-flush))
(define %fpipe-end%   (make-fpipe-end))
(define %fpipe-eof%   (eof-object))

(define-record-type <fpipe-error>
  (fpipe-error kind msg)
  fpipe-error?
  (kind  fpipe-error-type)
  (msg   fpipe-error-msg))

(define %all% (list %fpipe-reset% %fpipe-flush% %fpipe-end% %fpipe-eof%))

(define (fpipe-control? x)
  (or (eof-object? x)
      (and (struct? x) 
	   (or (fpipe-error? x)
	       (memq x %all%)))))

       
(define-record-type <fpipe>
  (make-fpipe% nin iin vin min nout Nout vout mout)
  fpipe?
  (nin  nin-ref  nin-set!)
  (iin  iin-ref  iin-set!)
  (vin  vin-ref  vin-set!)
  (min  min-ref  min-set!)
  (nout nout-ref nout-set!)
  (Nout Nout-ref Nout-set!)
  (vout vout-ref vout-set!)
  (mout mout-ref mout-set!))

(define nbuf (ash 1 17))
(define* (make-fpipe #:optional (bufflen nbuf))
  (make-fpipe%
   0
   0
   (make-bytevector bufflen)
   (make-channel)
   0
   0
   (make-bytevector bufflen)
   (make-channel)))

(define (fpipe-flush fpipe)
  (fpipe-put-u8 fpipe %fpipe-flush%))

(define (fpipe-end fpipe)
  (fpipe-put-u8 fpipe %fpipe-end%))


#|
Will return either
======================================
byte          : the-byte
eof           : in case of message end, make sure to transprt it to dependant
                pipes
<fpipe-error> : in case of error, make sure to transprt this object
                to dependant fpipes
<fpipe-fludh> : make sure to flush any dependant fpipes
======================================
|#


(define (fpipe-swap p1 p2)
  (let ((m2o (mout-ref p2))
	(m2i (min-ref  p2))
	(v2  (vin-ref  p2))
	(i2  (iin-ref  p2))
	(n2  (nin-ref  p2)))
    (define v (if (> n2 i2)
		  (begin
		    (put-message m2o (cons* n2 i2 v2))    
		    (get-message m2i))
		  v2))
      
    (let ((w (vout-ref p1))
	  (N (Nout-ref p1))
	  (i (nout-ref p1)))
      (vout-set! p1 v)
      (nout-set! p1 0)
      (Nout-set! p1 0)

      (vin-set! p2 w)
      (iin-set! p2 i)
      (nin-set! p2 N))))
      
(define (fpipe-get-u8 fpipe)
  (let ((N (Nout-ref  fpipe))	
	(i (nout-ref  fpipe))
	(v (vout-ref  fpipe)))
    
    (cond
     ;; Fast path, quickly return data from buffer
     ((< i N)
      (let ((r (bytevector-u8-ref v i)))
	(nout-set! fpipe (+ i 1))
	r))

     ;; Slow path Read new buffer form mout and then return budder to min
     (else
      (let ((r (get-message (mout-ref fpipe))))
	(match r
	 ((NN ii . vv)
	  (begin
	    (put-message (min-ref fpipe) v)
	    (Nout-set! fpipe NN)
	    (nout-set! fpipe ii )
	    (if (not (number? ii)) (error "aargh"))
	    (if (not (number? NN)) (error "aargh2"))
	    (vout-set! fpipe vv)
	    (fpipe-get-u8 fpipe)))
	 
	 (_
	  (when (not (fpipe-flush? r))
	    (nout-set! fpipe 0)
	    (Nout-set! fpipe 0))
	  
	  (put-message (min-ref fpipe) r)
	  
	  r)))))))

(define* (fpipe-get-bytevector fpipe bv
			       #:key (j 0) (len (bytevector-length bv)))
  
  (let* ((N  (Nout-ref  fpipe))
	 (i  (nout-ref  fpipe))
	 (v  (vout-ref  fpipe)))

    (let lp ((j j) (i i) (v v) (N N))
      (cond
       ((< j len)
	(cond
	 ((< i N)
	  (let ((k (min (- len j) (- N i))))
	    (bytevector-copy! v i bv j k)
	    (lp (+ j k) (+ i k) v N)))
	   
	 (else
	  (let ((r (get-message (mout-ref fpipe))))
	    (match r
	     ((NN ii . vv)
	      (begin
		(put-message (min-ref fpipe) v)
		(if (not (number? ii)) (error "pfft"))
		(lp j ii vv NN)))
	       	 
	     (_
	      (when (not (or (fpipe-flush? r) (fpipe-eof? r)))
		(set! j 0))
	      (Nout-set! fpipe 0)
	      (nout-set! fpipe 0)	      
	      (vout-set! fpipe v)
	      (put-message (min-ref fpipe) r)
	      
	      (values r j)))))))
	
       (else
	(if (not (number? i)) (error "xxx"))
	(if (not (number? N)) (error "xxx2"))
	(Nout-set! fpipe N)
	(nout-set! fpipe i)
	(vout-set! fpipe v)
	(values #f j))))))

(define* (fpipe-regenerate fpipe r)
  (match r
   ((N i . v)
    (if (not (number? i)) (error "buuuu"))
    (if (not (number? N)) (error "buuuu"))
    (put-message (min-ref fpipe) (vout-ref fpipe))
    (Nout-set! fpipe N)
    (nout-set! fpipe i)	      
    (vout-set! fpipe v))

   (_
    (Nout-set! fpipe 0)
    (nout-set! fpipe 0)
    (put-message (min-ref fpipe) r))))


(define (fpipe-peek-u8 fpipe)
  (let ((N (Nout-ref  fpipe))	
	(i (nout-ref  fpipe))
	(v (vout-ref  fpipe)))

    (cond
     ;; Fast path, quickly return data from buffer
     ((< i N)
      (let ((r (bytevector-u8-ref v i)))
	r))

     ;; Slow path Read new buffer form mout and then return budder to min
     (else
      (let ((r (get-message (mout-ref fpipe))))
	(match r
	 ((NN ii . vv)
	  (begin
	    (if (not (number? ii)) (error "666"))
	    (if (not (number? NN)) (error "666-2"))
	    (put-message (min-ref fpipe) v)
	    (Nout-set! fpipe NN)
	    (nout-set! fpipe ii)
	    (vout-set! fpipe vv)
	    (fpipe-peek-u8 fpipe)))
	 
	 (_
	  (nout-set! fpipe 0)
	  (Nout-set! fpipe 0)

	  (put-message (min-ref fpipe) r)
	  
	  r)))))))

(define (fpipe-put-u8 fpipe val)
  (let* ((v (vin-ref     fpipe))
	 (N (bytevector-length v))
	 (a (iin-ref     fpipe))
	 (i (nin-ref     fpipe)))
	
    (when (not (number? a)) (error "goopsie"))

    (define (get-new-vector-or-control)
      (let ((v (get-message (min-ref fpipe))))
	(if (bytevector? v)
	    (begin
	      (vin-set! fpipe v)
	      (iin-set! fpipe 0)
	      (nin-set! fpipe 0)
	      #f)	    
	    v)))

    (define (push-control val)
      (put-message (mout-ref fpipe) val)
      (get-new-vector-or-control))
    
    (define (flush)
      (put-message (mout-ref fpipe) (cons* i a v))
      (get-new-vector-or-control))
        
    (cond
     ((number? val)
      (cond
       ;; Fastpath, fill in buffer
       ((< i N)
	(bytevector-u8-set! v i val)
	(nin-set! fpipe (+ i 1)))

       ((fpipe-end? val)
	(fpipe-put-u8 fpipe val))

       ((or (fpipe-reset? val) (fpipe-error? val))
	(nin-set! fpipe 0)
	(fpipe-put-u8 fpipe val))
       
       (else
	(flush)
	(fpipe-put-u8 fpipe val))))

     ((fpipe-control? val)
      (cond
       ((= i 0)       
	(push-control val))
       
       (else
	(if (or (eof-object? val) (fpipe-flush? val))
	  (flush)
	  (nin-set! fpipe 0))
	
	(push-control val))))
          	
     (else
      (push-control
       (fpipe-error
	"fpipe-internal"
	(format #f "unknown fpipe val type: ~a" val)))))))

(define* (fpipe-put-bytevector fpipe val #:key (j 0) (len #f))
  (let* ((v (vin-ref           fpipe))
	 (N (bytevector-length v    ))
	 (a (iin-ref           fpipe))
	 (i (nin-ref           fpipe)))

    (define nv (if len len (bytevector-length val)))
    
    (when (not (number? a)) (error "goopsie"))  

    (let lp ((j j) (i i) (v v) (a a))
      (if (< j nv)
	  (cond
	   ((< i N)
	    (let ((m (min (- N i) (- nv j))))
	      (bytevector-copy! val j v i m) 
	      (lp (+ j m) (+ i m) v a)))
       
	   (else
	    (put-message (mout-ref fpipe) (cons* i a v))
	    (let ((v (get-message (min-ref fpipe))))
	      (if (bytevector? v)
		  (lp j 0 v 0)
		  (values)))))
	  (begin
	    (nin-set! fpipe i)
	    (iin-set! fpipe (if (number? a) a (error "woops")))
	    (vin-set! fpipe v))))))


(define (fpipe-handle-control)
  (let ((e #f))
    (values
     (case-lambda
      (()   e)
      ((ee) (set! e ee)))
     
     (lambda (out p lp reset flush eof)
       (define (push)
	 (cond
	  ((procedure? out)
	   (out p))
	  
	  ((channel? out)
	   (put-message out p))
	  
	  (else
	   (fpipe-put-u8 out p))))

       (cond
	((fpipe-reset? p)
	 (set! e #f)
	 (reset)
	 (push)
	 (lp))

	((fpipe-end? p)
	 (push))

	(e (lp))

	((eof-object? p)	 
	 (eof))

	((fpipe-flush? p)
	 (flush))
		
	((fpipe-error? p)
	 (set! e #t)
	 (push)
	 (lp)))))))

(define (fpipe-schemer p1 p2)
  (lambda (scm)
    (put-message p1 scm)

    (let lp ()
      (let ((x (get-message p2)))
	(cond
	 ((fpipe-error? x)
	  ;; reset pipeleine
	  (pk 'error x)
	  ;;(put-message p1 %fpipe-reset%)
	  
	  ;; pull out until we get an reset back
	  #;(let lp ()
	    (let ((y (get-message p2)))
	      (when (not (fpipe-reset? y))
		(lp))))

	  x)

	 ((fpipe-flush? x)
	  (if (fpipe-flush? x)
	      %fpipe-flush%
	      (lp)))

	 (else
	  x))))))

(define (tree-map f . trees)
  (let lp ((trees trees))
    (cond
     ((pair? (car trees))
      (cons (lp (map car trees)) (lp (map cdr trees))))
     
     ((null? (car trees))
      '())

     (else
      (apply f trees)))))

(define (tree-for-each f . trees)
  (let lp ((trees trees))
    (cond
     ((pair? (car trees))
      (lp (map car trees))
      (lp (map cdr trees)))
     
     ((null? (car trees))
      (values))

     (else
      (apply f trees)
      (values)))))

(define (tree-or f . trees)
  (let lp ((trees trees))
    (cond
     ((pair? (car trees))
      (or (lp (map car trees))
	  (lp (map cdr trees))))
     
     ((null? (car trees))
      #f)

     (else
      (apply f trees)))))

(define (tree-and f . trees)
  (let lp ((trees trees))
    (cond
     ((pair? (car trees))
      (and (lp (map car trees))
	   (lp (map cdr trees))))
     
     ((null? (car trees))
      #t)

     (else
      (apply f trees)))))

(define (tree-fold f s . trees)
  (let lp ((trees trees) (s s))
    (cond
     ((pair? (car trees))
      (lp (map cdr trees) (lp (map car trees) s)))
     
     ((null? (car trees))
      s)

     (else
      (apply f s trees)))))


(define *trace* #f)

(define* (generate-node f
			#:key
			(t1         #f)
			(t2         #f)
			(all?       #f)
			(1-to-n     #f)
			(n-to-1     #f)
			(parallel?  #f)
			(name       'anon))
  
  (define (trace . l)
    (when *trace*
      (apply pk name 't1 t1 't2 t2 l)))
  
  (define (er p)
    (error
     (format
      #f
      "wrong pipe type in generate-node (~a) t1 = ~a, t2 = ~a, p = ~a"
      name t1 t2
      (tree-map (lambda (p)
		  (if (or (channel? p) (fpipe? p))
		      (class-name (class-of p))
		      p))
		p))))
  
  (define (mk-pipe q)
    (cond
     ((eq? q #:fpipe)
      (make-fpipe))

     ((eq? q #:scm)
      (make-channel))

     (else
      (er q))))
      
  (define (get-type-0 one?)
    (lambda (p)
      ((if one? one? (lambda (x) x))
       (cond
	((channel? p)
	 #:scm)
	
	((fpipe? p)
	 #:fpipe)

	(else
	 (er p))))))

  (define (get-type p one?)
    (let/ec out
	    (tree-map (get-type-0 (if one? out #f)) p)))

  (define (check-0 p q)
    (cond
     ((eq? q #:fpipe)
      (when (not (fpipe? p))
	(er (cons q p))))
     
     ((eq? q #:scm)
      (when (not (channel? p))
	(er (cons q p))))
     
     (else
      (er (cons p q)))))

  (define (get-q p q)
    (if (pair? q)
	q
	(tree-map (lambda (p) q) p)))
  
  (define (check p q)
    (tree-for-each
     check-0
     p (get-q p q)))
     
    
  (define run
    (case-lambda
     (()
      (when (or 1-to-n n-to-1)
	(error "can't make a 1-to-n or n-to-1 fpipe node out of thin air"))

      (let ((tt1 (if t1 t1 #:scm)))
	(set! t1 tt1)
	(trace 'at_x)
	(let ((p1 (mk-pipe t1)))
	  (trace 'p1 (class-name (class-of p1)))
	  (run p1))))
     
     ((p1)
      (when (and 1-to-n (not t1))
	(error "can't make a 1-to-n fpipe node out of just the source"))

      (if t1
	  (check p1 t1)
	  (set! t1 (get-type p1 #f)))
      
      (define t
	(cond
	 (n-to-1
	  (if t2 t2 (get-type p1 #t)))

	 (1-to-n
	  t1)

	 (else
	  (tree-map
	   (lambda (p1 t2)
	     (if t2 t2 (get-type p1 #f)))
	   p1 (get-q p1 t2)))))

      (trace 't t)
     
      (define p2 (tree-map mk-pipe t))
      (trace 'p2 (class-name (class-of p2)))
      (set! t2 t)
      
      (run p1 p2))

     ((p1 p2)
      (trace 'check-t1 (class-name (class-of p1)))
      (if t1
	  (check p1 t1)
	  (set! t1 (get-type p1 #t)))

      (trace 'check-t2 (class-name (class-of p2)))
      (if t2
	  (check p2 t2)
	  (set! t2 (get-type p2 #f)))

      (trace 'got-types)
      
      ;; TODO Better default error handling here
      (define (F p1 p2 t1 t2)
	(trace 'F
	       'p1 (tree-map (lambda (x) (class-name (class-of x))) p1)
	       'p2 (tree-map (lambda (x) (class-name (class-of x))) p2))
	
	(define %loop (f p1 p2 t1 t2))

	(trace 'got-loop)
	
	(spawn-fiber
	 (lambda ()
	   (catch #t
	     %loop
	     (lambda x
	       (pk 'error-in name)
	       (pk x)
	       (values))
	     (lambda x
	       (backtrace))))
	 #:parallel? parallel?))

      (trace 'spawned-fiber)
      
      (cond
       (all? (F p1 p2 t1 t2))
       (else
	(cond
	 (1-to-n
	  (trace '1->n)
	  (tree-for-each F p1 p2 t1 t2))
	
	 (n-to-1
	  (trace 'n->1)
	  (tree-for-each (lambda (p2 p1 t1 t2) (F p1 p2 t1 t2))
			 p2 p1 t1 t2))
	
	 (else
	  (trace '1->1)
	  (tree-for-each F p1 p2 t1 t2)))))
      
      (trace 'out-of name)
      (values p1 p2))))
  
  run)

(define (id x) x)
