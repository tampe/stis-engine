;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes cond)
  #:use-module (oop goops)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (srfi srfi-9)
  #:use-module ((rnrs io ports) #:select (eof-object))
  #:use-module (ice-9 iconv)
  #:use-module (rnrs bytevectors)

  #:export (fpipe-cond))

(define (mk-pred preds)
  (let* ((first? #t)
	 (ret    (map (lambda (pred)
			(let ((active #f))
			  (lambda (last?)			    
			    (lambda (N i v)
			      (if N
				  (if first?
				      (if (pred N i v)
					  (begin
					    (set! active #t)
					    (set! first? #f)
					    #t)
					  #f)
				      active)
				  
				  (cond
				   ((eq? v %fpipe-eof%)
				    (let ((ret active))
				      (set! active #f)
				      (when (or ret last?)
					(set! first? #t))
				      ret))

				   ((memq v (list %fpipe-end% %fpipe-reset%))
				    (set! active #f)
				    (set! first? #t)
				    #t)

				   (else
				    (if (or first? active)
					#t
					#f))))))))
		      preds)))
    
    (let lp ((ret ret))
      (match ret
	(() '())
	((x) (list (x #t)))
	((x . l) (cons (x #f) (lp l)))))))

(define (mk-pred-scm preds)
  (map (lambda (pred)
	 (let ((active #t))
	   (lambda (v)
	     (cond
	      ((memq v (list %fpipe-end% %fpipe-reset%))
	       (set! active #f)
	       #t)

	      ((or (memq v (list %fpipe-flush%  %fpipe-eof%))
		   (fpipe-error? v))
	       (if active
		   #t
		   #f))
			      
	      (else
	       (if (pred v)
		   (begin
		     (set! active #t)
		     #t)
		   #f))))))
		      
       preds))

(define (%%fpipe-scm-cond p1 p2 lpred)
  (lambda ()
    (let* ((val #f)
	   (f   (lambda (x)
		  (let/ec out
		   (for-each
		    (lambda (pred p2)
		      (if (pred x)
			  (begin
			    (put-message p2 x)
			    (out #t))
			  #f))
		    lpred p2)))))
	  
      (let lp ()
	(let ((x (get-message p1)))
	  (f x)
	  (lp))))))

(define (%fpipe-scm-cond p1 p2 lpred)
  (%%fpipe-scm-cond p1 p2 (mk-pred-scm lpred)))

(define (%%fpipe-cond p1 p2 lpred)
  (lambda ()
    (define mout (mout-ref p1))
    
    (let lp ()
      (let* ((N (Nout-ref  p1))
	     (i (nout-ref  p1)))
	
	(define (f N i v)
	  (let/ec out
	   (for-each
	     (lambda (pred p)
	       (when (pred N i v)
		 (if N
		     (begin
		       (if (> (- N 1) (ash 1 12))
			   (fpipe-swap p1 p)
			   (begin
			     (fpipe-put-bytevector p v #:j i #:len N)
			     (nout-set! p1 N)))
		       
		       (out #t))
		     (fpipe-put-u8 p v))))
	     lpred p2)))
	(cond
	 ((< i N)
	  (f N i (vout-ref p1))
	  (lp))
	   
	 (else
	  (let ((x (get-message mout)))
	    (fpipe-regenerate p1 x)
	    (if (pair? x)
		(lp)
		(begin
		  (f #f #f x)
		  (lp))))))))))

(define (%fpipe-cond p1 p2 lpred)
  (%%fpipe-cond p1 p2 (mk-pred lpred)))

(define* (fpipe-cond lpred #:key (t #f) (parallel? #f))
  (generate-node
   (lambda (p1 p2 t1 t2)
     (if (eq? t1 #:scm)
	 (%fpipe-scm-cond p1 p2 lpred)
	 (%fpipe-cond     p1 p2 lpred)))
   #:parallel? parallel?
   #:t1         t
   #:1-to-n    #t
   #:name      'fpipe-cond))

