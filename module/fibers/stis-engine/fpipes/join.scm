;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes join)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (rnrs bytevectors)

  #:export (fpipe-join))

(define* (%fpipe-scm-join ps p2)
  (lambda ()
    (let* ((os (tree-map get-operation ps))
	   (op (apply choice-operation (tree-fold
					(lambda (s x) (cons x s))
					'() os))))
      (let lp ()
	(let ((x (perform-operation op)))
	  (put-message p2 x)
	  (lp))))))

(define (%fpipe-join ps p2)
  (lambda ()
    (let* ((us (tree-map mout-ref ps))
	   (os (tree-map (lambda (mout fpipe)
			   (let ((o (get-operation mout)))
			     (wrap-operation
			      o
			      (lambda (x)
				(list x fpipe)))))
			 us ps))
	   (op (apply choice-operation (tree-fold
					(lambda (s x)
					  (cons x s))
					'() os))))
      
      (let lp ()
	(let* ((Ns (tree-map Nout-ref ps))
	       (is (tree-map nout-ref ps))
	       (vs (tree-map vout-ref ps)))
	
	  (cond
	   ((tree-or	     
	     (lambda (fpipe N i v)
	       (if (< i N)
		   (list fpipe N i v)
		   #f))
	     ps Ns is vs)
	    =>
	    (lambda (x)
	      (match x
	       ((fpipe N i v)
		(if (> (- N i) (ash 1 12))
		    (fpipe-swap fpipe p2)
		    (begin
		      (fpipe-put-bytevector p2 v #:j i #:len N)
		      (nout-set! fpipe N)))
		(lp)))))

	   (else
	    (let ((x (perform-operation op)))
	      (match x
	       ((x fpipe)
		(fpipe-regenerate fpipe x)	      
		(when (not (pair? x))
		  (fpipe-put-u8 p2 x))))
	      (lp)))))))))

(define* (fpipe-join #:key (t2 #f))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (equal? (car q1) q2))
       (error "cond clause does only support the same input as output"))
     (if (eq? q2 #:scm)
	 (%fpipe-scm-join p1 p2)
	 (%fpipe-join     p1 p2)))

   #:t2     t2
   #:n-to-1 #t
   #:name   'fpipe-join))
