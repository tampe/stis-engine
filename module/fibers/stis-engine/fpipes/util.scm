;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes util)
  #:use-module (oop goops)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (fibers stis-engine fpipes functional)
  #:use-module (rnrs bytevectors)
  
  #:export (fpipe-id
	    fpipe-demuplex
	    fpipe-multiplex
	    fpipe-pk	    
	    fpipe-tee
	    fpipe-null
	    fpipe-skip
	    fpipe-count
	    fpipe-prepend))


;; ============================ FPIPE-PK ================================
(define (%fpipe-pk p1 p2 info)
  (define buf (make-bytevector 10))
  (lambda ()
    (let lp ()
      (aif (e j) (fpipe-get-bytevector p1 buf)
	   (begin
	     (pk (info) e j buf)
	     (fpipe-put-bytevector p2 buf #:len j)
	     (fpipe-put-u8 p2 e))
	   
	   (begin
	     (pk (info) j buf)
	     (fpipe-put-bytevector p2 buf #:len j)))
      (lp))))

(define (%fpipe-scm-pk p1 p2 info)
  (lambda ()
    (let lp ()
      (put-message p2 (pk (info) (get-message p1)))
      (lp))))

(define-syntax-rule (fpipe-pk info . l) (!fpipe-pk (lambda () info) . l))
  
(define* (!fpipe-pk info #:optional (t #f))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (cond
      ((eq? q1 #:scm)
       (%fpipe-scm-pk p1 p2 info))
      
      ((eq? q1 #:fpipe)
       (%fpipe-pk p1 p2 info))
      
      (else
       (error (format #f "wrong type in fpipe-pk ~a" q1)))))
   #:t1 t
   #:name 'fpipe-pk))

;; ================================ FPIPE ID ===========================
(define (%fpipe-scm-id p1 p2)
  (lambda ()
    (let lp ()
      (put-message p2 (get-message p1))
      (lp))))

(define (%fpipe-id p1 p2)
  (define mout (mout-ref p1))

  (lambda ()
    (let lp ()
      (let ((N (Nout-ref p1))
	    (i (nout-ref p1)))
	(cond
	 ((= i N)
	  (let ((x (get-message mout)))
	    (fpipe-regenerate p1 x)
	    (when (not (pair? x))
	      (fpipe-put-u8 p2 x))
	    (lp)))
	 
	 ((> (- N i) (ash 1 10))
	  (fpipe-swap p1 p2)
	  (lp))
	 
	 (else
	  (let ((v (vout-ref p1)))
	    (fpipe-put-bytevector p2 v #:j i #:len N)
	    (if (not (number? N)) (error "o"))
	    (nout-set! p1 N)
	    (lp))))))))

(define* (fpipe-id #:optional (t #f))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (eq? q1 q2))
       (error "fipe-id demands equal queue in as out"))

     (if (eq? q1 #:scm)
	 (%fpipe-scm-id p1 p2)
	 (%fpipe-id     p1 p2)))
   #:t1 t
   #:name "<id>"))


;; ========================= FPIPE PLEXES ============================
(define (fpipe-demuplex selector)
  (generate-node   
   (lambda (p1 ps q1 q2)
     (define n   (length ps))
     (define v   (list->vector ps))
     (define get (if (eq? q1 #:scm) get-message fpipe-get-u8))
     (define put (if (eq? q2 #:scm) put-message fpipe-put-u8))

     (lambda ()
       (let lp ()
	 (let ((x (get p1)))
	   (aif i (selector x)
		(begin
		  (if (fpipe-control? x)	       
		      (let lp2 ((l ps))
			(if (pair? l)
			    (begin
			      (put (car l) x)
			      (lp2 (cdr l)))
			    (values)))
		      (put (vector-ref v i) x))
		  (lp))
		(values))))))

   #:1-to-n #t))

(define (fpipe-multilex combiner)
  (generate-node   
   (lambda (ps p2 q1 q2)
     (define n   (length ps))
     (define v   (list->vector ps))
     (define get (if (eq? q1 #:scm) get-message fpipe-get-u8))
     (define put (if (eq? q2 #:scm) put-message fpipe-put-u8))

     (set! combiner (combiner n))
     
     (lambda ()
       (let lp ((i 0))
	 (if (< i 0)
	     (let ((x (get (vector-ref ps i))))
	       (aif it (combiner x)
		    (begin
		      (put p2 it)
		      (lp (+ i 1)))
		    (values)))
	     (lp 0)))))
	     
   #:n-to-1 #t))

;; =============================== FPIPE TEE ===========================      

(define (%fpipe-tee p1 ps)
  (lambda ()
    (define mout (mout-ref p1))
    
    (let lp ()
      (let ((i (nout-ref p1))
	    (N (nout-ref p1)))
	(if (= i N)
	    (let ((x (get-message mout)))
	      (fpipe-regenerate p1 x)
	      (when (not (pair? x))
		(for-each (lambda (p) (fpipe-put-u8 p x)) ps))
	      (lp))

	    (let ((v (vout-ref p1)))
	      (for-each
	       (lambda (p)
		 (fpipe-put-bytevector p v #:j i #:len N))
	       ps)
	      (if (not (number? N)) (error "r"))
	      (nout-set! p1 N)
	      (lp)))))))

(define (%fpipe-scm-tee p1 ps)
  (lambda ()
    (let lp ()
      (let ((x (get-message p1)))
	(for-each (lambda (p) (put-message p x)) ps)
	(lp)))))


(define* (fpipe-tee #:optional (t #f))
  (generate-node
   (lambda (p1 ps q1 qs)
     (when (not (eq? q1 qs))
       (error "fpipe-tee only workd for same input queue as output queue"))
     
     (if (equal? q1 #:scm)
	 (%fpipe-scm-tee p1 ps)
	 (%fpipe-tee p1 ps)))

   #:t1      t
   #:1-to-n #t))

;; ============================ FPIPE NULL =========================
(define (%fpipe-null p1 p2 silence?)
  (lambda ()
    (define mout (mout-ref p1))
    
    (let lp ()
      (let ((i (nout-ref p1))
	    (N (nout-ref p1)))
	(if (= i N)
	    (let ((x (get-message mout)))
	      (fpipe-regenerate p1 x)
	      (when (and silence? (not (pair? x)))
		(fpipe-put-u8 p2 x))
	      (lp))
	      
	    (let ()
	      (if (not (number? N)) (error "e"))
	      (nout-set! p1 N)
	      (lp)))))))


(define (%fpipe-scm-null p1 p2 silence?)
  (lambda ()
    (let lp ()
      (let ((x (get-message p1)))
	(when (and silence? (complex? x))
	  (put-message p2 x)
	  (lp))))))

(define* (fpipe-null #:key (silence? #t))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (eq? q1 q2))
       (error "fpipe-null does only supprt the same input as output type"))

     (if (eq? q1 #:scm)
	 (%fpipe-scm-null p1 p2 silence?)
	 (%fpipe-null     p1 p2 silence?)))))

(define (list->bytevector l)
  (let ((bv (make-bytevector (length l))))
    (let lp ((l l) (i 0))
      (define (norm x) (if x x 0))
      (if (pair? l)
	  (begin
	    (bytevector-u8-set! bv i (norm (car l)))
	    (lp (cdr l) (+ i 1)))
	  bv))))

;; =========================== FPIPE PREPEND ===============================
(define-syntax-rule (fpipe-prepend bv ...)
  (!fpipe-prepend (lambda () (list bv ...))))

(define (!fpipe-prepend bb)
  (define n (length (bb)))
  
  (define (bv)
    (list->bytevector (bb)))
    
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (and (eq? q1 #:fpipe) (eq? q2 #:fpipe)))
       (error "prepend is only for fpipe in an dout queues"))
     
     (lambda ()
       (define bv2    (make-bytevector (ash 1 12)))
       (define first? #t)
       (define mout (mout-ref p1))
       (define e    #f)
       
       (let lp ()
	 (define N (Nout-ref p1))
	 (define i (nout-ref p1))
	 (define m (if first? n 0))
	 (define v (vout-ref p1))
	 
	 (aif (ctrl j) (fpipe-get-bytevector p1 bv2)
	      (if e
		  (if (eq? ctrl %fpipe-reset%)
		      (begin
			(set! first? #t)
			(set! e      #f)
			(lp))
		      (lp))
		  
		  (cond
		   ((eq? ctrl %fpipe-flush%)
		    (fpipe-put-u8 p2 ctrl)
		    (lp))
		       
		   ((fpipe-error? ctrl)
		    (fpipe-put-u8 p2 ctrl)
		    (set! e #t)
		    (lp))

		   ((eq? ctrl %fpipe-reset%)
		    (fpipe-put-u8 p2 ctrl)
		    (set! first? #t)
		    (lp))

		   ((eq? ctrl %fpipe-end%)
		    (fpipe-put-u8 p2 ctrl)
		    (values))

		   ((eq? ctrl %fpipe-eof%)
		    (when first?
		      (fpipe-put-bytevector p2 (bv) #:len m))
		    (fpipe-put-bytevector p2 bv2 #:len j)
		    (fpipe-put-u8 p2 ctrl)
		    (set! first? #t)
		    (lp))))
		  
	      (begin
		(when first?
		  (fpipe-put-bytevector p2 (bv) #:len m)
		  (set! first? #f))
		(fpipe-put-bytevector p2 bv2 #:len j)
		(lp))))))
   
   #:name 'fpipe-prepend
   #:t1   #:fpipe   
   #:t2   #:fpipe))


(define-syntax-rule (fpipe-skip n) (!fpipe-skip (lambda () n)))

(define (!fpipe-skip m)
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (and (eq? q1 #:fpipe) (eq? q2 #:fpipe)))
       (error "skip is only for fpipe in an dout queues"))
     
     (lambda ()
       (define mout (mout-ref p1))
       (define first #t)       
       (let lp ()
	 (let ((i (nout-ref p1))
	       (N (Nout-ref p1)))
	   (cond
	    ((= i N)
	     (let ((x (get-message mout)))
	       (fpipe-regenerate p1 x)
	       (if (not (pair? x))
		   (begin
		     (when (not (eq? x %fpipe-flush%))
		       (set! first #t))
		     (fpipe-put-u8 p2 x)
		     (when (not (eq? x %fpipe-end%))
		       (lp)))
		   (lp))))

	    ((> (- N i) (ash 1 10))
	     (when first	       
	       (nout-set! p1 (+ i (m))))
	     (set! first #f)
	     (fpipe-swap p1 p2)
	     (lp))
	    
	    ((< i N)
	     (when first
	       (set! i (+ i (m))))
	     (set! first #f)
	     (fpipe-put-bytevector p2 (vout-ref p1) #:j i #:len N)
	     (if (not (number? N)) (error "x"))
	     (nout-set! p1 N)
	     (lp)))))))
	       
   #:t1   #:fpipe
   #:t2   #:fpipe
   #:name 'fpipe-skip))

(define (fpipe-count)
  (fpipe-scm-filter (lambda (N i v s) (+ s (- N i))) 0))
