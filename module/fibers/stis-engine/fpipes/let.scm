;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes let)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (srfi srfi-9)
  #:use-module ((rnrs io ports) #:select (eof-object))
  #:use-module (ice-9 iconv)
  #:use-module (rnrs bytevectors)

  #:export (fpipe-let))


(define (%fpipe-scm-let p1 p2 lset thunk)
  (lambda ()
    (let lp ()
      (let ((x (get-message p1)))
	(when (not (fpipe-control? x))
	  ((car lset) x)
	  (thunk))
	(put-message p2 x)
	(lp)))))

(define (%fpipe-let p1 p2 lset thunk)
  (lambda ()
    (define mout   (mout-ref p1))  
    (define first? #t)

    (let lp ()
      (let ((N (Nout-ref p1))
	    (i (nout-ref p1)))
	(if (< i N)
	    (let ((v (vout-ref p1))
		  (m (- N i)))
	      (when first?
		(let lp2 ((l lset) (j 0))
		  (if (pair? l)
		      (if (< j m)
			  (begin
			    ((car l) (bytevector-u8-ref v (+ i j)))
			    (lp2 (cdr l) (+ j 1)))
			  (error
			   "fpipe let cant be done due to too little data"))
		      (thunk))))

	      (if (> (- N i) (ash 1 1))
		  (fpipe-swap p1 p2)
		  (begin
		    (fpipe-put-bytevector p2 v #:j i #:len N)
		    (nout-set! p1 N)))
	      (set! first? #f)
	      (lp))
	      
	    (let ((x (get-message mout)))	      
	      (fpipe-regenerate p1 x)
	      (when (fpipe-control? x)
		(when (not (eq? x %fpipe-reset%))
		  (set! first? #t))
		(fpipe-put-u8 p2 x))
	      (lp)))))))
	      
			  
	
(define* (fpipe-let lset thunk)
  (generate-node
   (lambda (p1 p2 q1 q2)
     (when (not (eq? q1 q2))
       (error "let clause does only support the same input as output"))

     (if (eq? q1 #:scm)
	 (%fpipe-scm-let p1 p2 lset thunk)
	 (%fpipe-let     p1 p2 lset thunk)))

   #:name 'fpipe-let))
