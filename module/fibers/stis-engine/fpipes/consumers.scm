;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes consumers)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module ((rnrs io ports) #:select (eof-object))
  #:use-module (ice-9 iconv)
  #:use-module (rnrs bytevectors)
  
  #:export (fpipe->x	    
	    fpipe-collector
	    
	    fpipe->list
	    fpipe->bv
	    fpipe->string
	    fpipe->scm
	    ))

(define (fpipe->x final)
  (lambda (fpipe)
    (let lp ((l '()) (x (fpipe-get-u8 fpipe)))
      (define (next l)
	(lp l (fpipe-get-u8 fpipe)))
    
      (cond
       ((eof-object? x)
	(final l))
     
       ((or (fpipe-flush? x) (fpipe-reset? x))
	(next l))
     
       ((fpipe-error? x)
	x)

       (else
	(next (cons x l)))))))
	     

(define (list->bv l)
  (define n  (length l))
  (define bv (make-bytevector n))
  (let lp ((i 0) (l (reverse! l)))
    (if (< i n)
	(begin
	  (bytevector-u8-set! bv i (car l))
	  (lp (+ i 1) (cdr l)))
	bv)))

(define (list->string l)
  (bytevector->string (list->bv l) "UTF-8"))

(define (list->scm l)
  (with-input-from-string (list->string l) read))

(define (fpipe-collector fpipe->x)
  (lambda ()
    (generate-node
     (lambda (p0 p1 q0 q1)
       (when (not (and (eq? q0 #:fpipe) (eq? q1 #:scm)))
	 (error "fpipe-collector does not has fpipe as in and scm as out"))
     
       (lambda ()
	 (let lp ()
	   (let ((x (fpipe->x p0)))
	     (put-message p1 x)
	     (when (not (fpipe-end? x))
	       (lp))))))
   
     #:t1 #:fpipe
     #:t2 #:scm

     #:name (format #f "<consumer ~a>" (procedure-name fpipe->x)))))

(define fpipe->list    (fpipe-collector (fpipe->x reverse!)))
(define fpipe->bv      (fpipe-collector (fpipe->x list->bv)))
(define fpipe->string  (fpipe-collector (fpipe->x list->string)))
(define fpipe->scm     (fpipe-collector (fpipe->x list->scm)))
