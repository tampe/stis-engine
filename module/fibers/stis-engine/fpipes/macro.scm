;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes macro)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (fibers stis-engine fpipes util)
  #:use-module (fibers stis-engine fpipes cond)
  #:use-module (fibers stis-engine fpipes join)
  #:use-module (fibers stis-engine fpipes let)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (rnrs bytevectors)
  #:export (fpipe-X
	    define-fpipe-construct
	    fpipe-construct
	    fpipe-construct-0))

(define-syntax pk-mac
  (lambda (x) 
    (syntax-case x ()
      ((_ . x)
       ;(pk 'in-mac (syntax->datum #'x))
       #t))))

(define-syntax-rule (fpipe-X (code . a))
  (begin
    (pk-mac fpipe-X code)
    (lambda (p)
      (tree-map (lambda (pp) (code pp . a)) p))))

(define-syntax let-mac
  (syntax-rules ()
    ((_ p ((it ...) (a b) ...) . l)
     (let ((it #f) ... (a #f) ...)	   
       (define-values (p* p2)
	 ((fpipe-let
	   (list (lambda (x) (set! it x)) ...)
	   (lambda () (values) (set! a b) ...)) p))
       (fpipe-construct-0 p2 . l)))
        
    ((_ p (it       (a b) ...) . l)    
     (let ((it #f) (a #f) ...)
       (define-values (p* p2)
	 ((fpipe-let 
	   (list (lambda (x) (set! it x)))
	   (lambda () (values) (set! a b) ...)) p))
       (fpipe-construct-0 p2 . l)))))


(define-syntax cond-mac
  (syntax-rules ()
    ((_ (p . l) ...)
     (begin
       (pk-mac cond-mac-0)
       (let ((pp (make-channel)))
	 (cond-mac pp (p . l) ...))))

    ((_ #:scm (p . l) ...)
     (let ((pp (make-channel)))
       (cond-mac pp (p . l) ...)))

    ((_ #:fpipe (p . l) ...)
     (let ((pp (make-fpipe)))
       (cond-mac pp (p . l) ...)))

    ((_ pp (p . l) ...)
     (begin
       (pk-mac cond-mac)
       ((fpipe-X (cond-mac2 (p . l) ...)) pp)))))

(define (matcher x)
  (lambda (N i v)
    (let* ((y x)
	   (n (bytevector-length y)))
      (let lp ((j 0) (ii i))
	(if (< j n)
	    (if (< ii N)
		(if (eq? (bytevector-u8-ref v ii)
			 (bytevector-u8-ref y j))
		    (lp (+ j 1) (+ ii 1))
		    #f)
		#f)
	    #t)))))

(define-syntax cond-branch
  (syntax-rules (else)
    ((_ p1 else . l)
     (cond-branch p1 #t . l))

    ((_ p1 #:match m . l)
     (let ()
       (define p2 (fpipe-construct-0 p1 . l))
       (values p2 (matcher m))))
    
    ((_ p1 pred . l)
     (values
      (fpipe-construct-0 p1 . l)
      (lambda x pred)))))

(define (get-type p1 p2)
  (let/ec out
    (tree-map
     (lambda (p1 p2)
       (out
	(tree-map
	 (lambda (p)
	   (if (channel? p)
	       #:scm
	       #:fpipe))
	 p2)))
     p1 p2)))

(define-syntax cond-mac2
  (lambda (x)        	  
    (syntax-case x ()
      ((_ p1 a ...)
       (with-syntax (((p3 ...) (generate-temporaries #'(a ...)))
		     ((pp ...) (generate-temporaries #'(a ...)))
		     ((f  ...) (generate-temporaries #'(a ...))))
	  #'(let ()
	      (define pp
		(if (channel? p1)
		    (make-channel)
		    (make-fpipe)))
	      ...
		
	      (define-values (p3 f) (cond-branch pp . a))
	      ...
	      (define p32 (list p3 ...))
	      (define p22 (list pp ...))
	      
	      ((fpipe-cond (list f ...)) p1 p22)
	      (let ()		
		(define-values (p* p**)
		  ((fpipe-join #:t2 (get-type p22 p32)) p32))
		(values p1 p**))))))))

	     

    
    
(define-syntax fpipe-construct
  (syntax-rules (cond let if when begin)
    ((_ (begin a ...) . l)
     (fpipe-construct a ... . l))
    
    ((_ (let ((it ...) (a v) ...) code ...) . l)
     (let ()
       (define p1 (make-fpipe))
       (define p2 ((fpipe-X (let-mac ((it ...) (a v) ...) code ...)) p1))
       (values p1 (fpipe-construct-0 p2 . l))))

    ((_ (let (it (a v) ...) code ...) . l)
     (let ()
       (define p1 (make-channel))
       (define p2 ((fpipe-X (let-mac (it (a v) ...) code ...)) p1))
       (values p1 (fpipe-construct-0 p2 . l))))
    
    ((_ (cond . a) . l)
     (let ()
       (pk-mac found-cond)
       (define-values (p1 p2)
	 (cond-mac . a))
	   
       (values p1 (fpipe-construct-0 p2 . l))))
    
    ((_ (if p a) . l)
     (fpipe-construct (when p a) . l))

    ((_ (if p a b) . l)
     (fpipe-construct (cond (p a) (else b)) . l))

    ((_ (when p . a) . l)
     (fpipe-construct (cond (p . a) (else)) . l))
    
    ((_)
     ((fpipe-id)))
      
    ((_ f . l)
     (let ()
       (define-values (p1 p2) (f))
       (values p1 (fpipe-construct-0 p2 . l))))))

(define-syntax fpipe-construct-0
  (syntax-rules (cond let if when begin)
    ((_ p (begin a ...) . l)
     (fpipe-construct-0 p a ... . l))
    
    ((_ p (let as code ...) . l)
     (let ()
       (define p2 ((fpipe-X (let-mac as code ...)) p))
       (fpipe-construct-0 p2 . l)))

    ((_ p (cond . a) . l)
     (let ()
       (define-values (p* p2) (cond-mac p . a))	   
       (fpipe-construct-0 p2 . l)))
    
    ((_ p1 (if p a) . l)
     (fpipe-construct-0 p1 (when p a) . l))

    ((_ p1 (if p a b) . l)
     (fpipe-construct-0 p1 (cond (p a) (else b)) . l))

    ((_ p1 (when p . a) . l)
     (fpipe-construct-0 p1 (cond (p . a) (else)) . l))
    
    ((_ p1)
     (let ()
       (define-values (p* p2) ((fpipe-id) p1))
       p2))

    ((_ p1 f)
     (let ()
       (define-values (p* p2) (f p1))
       p2))
      
    ((_ p1 f . l)
     (let ()
       (define-values  (p* p2) (f p1))
       (fpipe-construct-0 p2 . l)))))


(define-syntax-rule (define-fpipe-construct t1 fkn . l)
  (define fkn
    (define out
      (case-lambda
       (()
	(let ((p1 (if (eq? t1 #:scm)
		      (make-channel)
		      (make-fpipe))))
	  (out p1)))

       ((p1)
	(values
	 p1
	 (fpipe-construct-0 p1 . l)))))
    out))
