;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes functional)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module ((rnrs io ports) #:select (eof-object))
  #:use-module (ice-9 iconv)
  #:use-module (rnrs bytevectors)

  #:export (;; Higher order do do
	    fpipe-message-fold
	    fpipe-scm-filter
	    fpipe-map
	    fpipe-fold))

(define* (fpipe-scm-filter F s)
  (generate-node
   (lambda (p1 p2 q1 q2)
     (lambda ()
       (define mout (mout-ref p1))
     
       (let lp ((ss s))
	 (let ((i (nout-ref p1))
	       (N (Nout-ref p1)))
	   (if (< i N)
	       (begin
		 (if (not (number? N)) (error "q"))
		 (nout-set! p1 N)
		 (lp (F N i (vout-ref p1) ss)))
	       (let ((x (get-message mout)))
		 (fpipe-regenerate p1 x)
		 (if (pair? x)
		     (lp ss)
		     (cond
		      ((eq? x %fpipe-eof%)
		       (put-message p2 ss)
		       (lp s))

		      ((eq? x %fpipe-flush%)
		       (put-message p2 x)
		       (lp ss))

		      (else
		       (put-message p2 x)
		       (lp s))))))))))
   #:t1 #:fpipe
   #:t2 #:scm
   #:name 'fpipe-scm-filter))

(define (fpipe-message-fold F s)
  (generate-node
   (lambda (p1 p2 q1 q2)
     (lambda ()
       (define mout (mout-ref p1))
     
       (let lp ((ss s))
	 (let ((x (fpipe-get-u8 p1)))
	   (if (number? x)
	       (let ((y (F x ss)))
		 (fpipe-put-u8 p2 y)
		 (lp y))
	       (cond
		((eq? x %fpipe-flush%)
		 (fpipe-put-u8 p2 x)
		 (lp ss))

		(else
		 (fpipe-put-u8 p2 x)
		 (lp s))))))))
   #:t1   #:fpipe
   #:t2   #:fpipe
   #:name 'fpipe-message-fold))
	
(define* (fpipe-map F1 #:key (F2 (lambda (x) x)) (t #:scm))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (define get (if (eq? q1 #:scm) get-message fpipe-get-u8))
     (define put (if (eq? q2 #:scm) put-message fpipe-put-u8))
     (lambda ()    
       (let lp ()
	 (let ((tree (tree-map (lambda (p) (get p)) p1)))
	   (put
	    p2
	    (tree-map
	     (lambda (z x)
	       (if (fpipe-control? x) 
		   (F2 x)
		   (F1 x)))
	     p1 tree))
	   (lp)))))
   #:t1 t))

(define* (fpipe-fold F s #:optional (t #:scm))
  (generate-node
   (lambda (p1 p2 q1 q2)
     (define get (if (eq? q1 #:scm) get-message fpipe-get-u8))
     (define put (if (eq? q2 #:scm) put-message fpipe-put-u8))
  
     (lambda ()    
       (let lp ()
	 (let ((ss (tree-fold (lambda (s p) (F (get p) s)) s p1)))
	   (put p2 ss)))))
   #:t1 t))

