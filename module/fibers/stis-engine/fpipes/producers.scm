;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes producers)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (ice-9 iconv)
  #:use-module (rnrs bytevectors)
  
  #:export (scmx->fpipe
	    
	    list->fpipe
	    bv->fpipe
	    string->fpipe
	    scm->fpipe
	    ))

(define (scmx->fpipe x->list)
  (lambda ()
    (generate-node
     (lambda (p1 p2 q1 q2)
       (when (not (and (eq? q1 #:scm) (eq? q2 #:fpipe)))
	 (error "fpipe-producer does not has scm as in and fpipe as out"))

       (lambda ()
	 (let lp ()
	   (let ((x (get-message p1)))
	     (cond
	      ((or (fpipe-reset? x) (fpipe-end? x) (fpipe-flush? x))
	       (fpipe-put-u8 p2 x))
	      (else
	       (let lp2 ((l (x->list x)))
		 (if (pair? l)
		     (begin
		       (fpipe-put-u8 p2 (car l))
		       (lp2 (cdr l)))
		     (begin
		       (fpipe-put-u8 p2 %fpipe-eof%)
		       (lp))))))))))
     #:t1 #:scm
     #:t2 #:fpipe
     #:name (format #f "<producer ~a>" (procedure-name x->list)))))

(define (id x) x)
(define (bv->list bv)
  (define n (bytevector-length bv))
  
  (let lp ((i 0) (l '()))
    (if (< i n)
	(lp (+ i 1) (cons (bytevector-u8-ref bv i) l))
	(reverse! l))))

(define (string->list str)
  (bv->list
   (string->bytevector str "UTF-8")))

(define (scm->list scm)
  (string->list
   (with-output-to-string
    (lambda () (write scm)))))

(define list->fpipe   (scmx->fpipe id))
(define bv->fpipe     (scmx->fpipe bv->list))
(define string->fpipe (scmx->fpipe string->list))
(define scm->fpipe    (scmx->fpipe scm->list))
