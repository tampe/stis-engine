;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine q)
  #:use-module (fibers stis-engine fpipes)
  #:use-module (fibers stis-engine tools zmq)
  #:use-module (fibers stis-engine zmq zmq)
  #:use-module (fibers stis-engine tools c-atom)
  #:use-module (fibers stis-engine tools zlib)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (rnrs bytevectors)
  #:export     (run-server make-client test-q))

#| 
  zmq client and server bytestreams with optional gzipping and
  usage of c-based serializer and deserializer.
|#

(define-fpipe-construct #:scm (zmq->atom socket)
  (zmq->fpipe socket)
  (let ((it)
	(cmp?  (= 1 (logand it 1)))
	(text? (= 2 (logand it 2))))

    (fpipe-skip 1)

    (when cmp?
      (uncompress-from-fpipe-to-fpipe))

    (if text?
	(fpipe->scm)
	(fpipe->c-atom))))

(define-fpipe-construct #:scm (atom->zmq socket)
  (let ( it
	 (header (car it))
	 (cmp?   (memq 'compress header))
	 (text?  (memq 'text     header))
	 (tag    (logior (if cmp? 1 0) (if text? 2 0))))

    (if text?
	(scm->fpipe)
	(c-atom->fpipe))
    
    (when cmp?
      (compress-from-fpipe-to-fpipe #:level 3))
 
    (fpipe-prepend tag)

    (fpipe->zmq socket)))

(define* (make-client address ip-server? #:key (context #f))
  (define ctx    (if context context (zmq-init)))
  (define socket (zmq-socket ctx ZMQ_REQ))
  
  (if ip-server?
      (zmq-bind    socket address)
      (zmq-connect socket address))
  
  (define-values (ch1 ch2)  
    (fpipe-construct
     (atom->zmq socket)
     (zmq->atom socket)))
     
  (define action (fpipe-schemer ch1 ch2))
  (lambda* (message #:key (compress? #f) (text? #t))
	   (cdr (action (cons (append (if compress? '(compress) '())
				      (if text?     '(text)     '()))
			      message)))))
  

(define* (run-server server-lambda address ip-server? #:key (context #f))
  (define ctx    (if context context (zmq-init)))
  (define socket (zmq-socket ctx ZMQ_REP))
  (define (lam x)
    (call-with-values (lambda () (server-lambda (cdr x)))
      (lambda* (message #:key (compress? #f) (text? #t))
	 (cons (append (if compress? '(compress) '())
		       (if text?     '(text)     '()))
	       message))))
			
  (if ip-server?
      (zmq-bind    socket address)
      (zmq-connect socket address))
  
  (define-values (ch1 ch2)  
    (fpipe-construct
     (zmq->atom socket)
     (fpipe-map lam)
     (atom->zmq socket)))

  (define schemer (fpipe-schemer ch1 ch2))
  
  (spawn-fiber
   (lambda ()
     (let lp ()
       (schemer %fpipe-eof%)
       (lp)))
   #:parallel? #f))
    
;; application server config
;; TEST TOOL

;;(define v (iota 2000000))
(define v (make-bytevector 400000000 0))

(define (test-q)
  (run-fibers
   (lambda ()
     ;; In order to be able to use threads we need to have a common context
     (define context (zmq-init))
     
     (define client (make-client "inproc://a" #f
				 #:context context))
     
     (run-server (lambda (x) x) "inproc://a" #t
		 #:context context)

     (pk 'client (client (iota 3)))
     (pk 'client (client (iota 2)))
     (pk 'client (bytevector-length (client v #:compress? #f))))))

    
    
    
  
