;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes var)
  #:use-module (fibers stis-engine fpipes fpipes)
  #:use-module (fibers stis-engine fpipes atom)
  #:use-module (data stis-data var)
  #:use-module (fibers)

  #:export (var->fpipe
	    fpipe->var

	    ;;test tool
	    test-var-fpipe))

(define-values
    (write-data-v-fpipe
     reg-set-var-v-fpipe
     reg-set-car-v-fpipe
     reg-set-cdr-v-fpipe
     reg-set-vector-v-fpipe
     reg-set-slot-v-fpipe
     reg-set-struct-v-fpipe
     reg-set-hashq-v-fpipe
     reg-set-hash-v-fpipe

     var-dump-fpipe
     var-load-fpipe)
  
  (mk-var write-tag-fpipe
	  read-tag-fpipe
	  read-atom-fpipe
	  write-atom-fpipe
	  map-fpipe))

(define-values (var->fpipe fpipe->var)
  (mk-scm-fpipe var-dump-fpipe var-load-fpipe))

;; ========================== TEST TOOL ===========================
(define (test-var-fpipe)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2) (fpipe-construct (var->fpipe)
					       #:id #:id #:id
					       (fpipe->var)))
     
     (define scm-schemer  (fpipe-schemer ch1 ch2))
     (define v            (list 1 1 1 1))
     (set-car! v v)
     
     (pk 'scm-schemer-1  (scm-schemer  '((a b) 2 3)))
     (pk 'scm-schemer-2  (scm-schemer v))
     (pk 'scm-schemer-3  (length (scm-schemer (iota 1000000)))))))

