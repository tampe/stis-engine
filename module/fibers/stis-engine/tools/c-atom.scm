;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine tools c-atom)
  #:use-module ( (persist persistance)
  	       	 #:select
		 ( atom-c-serialize
		   atom-c-deserialize
	    	   atom-load-bv))
		 
  #:use-module (rnrs bytevectors)
  #:use-module (fibers)
  #:use-module (fibers channels)

  #:use-module (fibers stis-engine fpipes)
  #:use-module ((fibers stis-engine tools atom)
		#:select
		(mk-scm->fpipe
		 mk-fpipe->scm))

  #:use-module (ice-9 match)

  #:export (;; high level
	    c-atom->fpipe
	    fpipe->c-atom
	    
	    ;;test tool
	    test-c-atom-fpipe))

(define-syntax aif
  (syntax-rules ()
    ((_ (it . l) p a b)
     (call-with-values (lambda () p)
       (lambda (it . l)
	 (if it a b))))
    
    ((_ it p a b)
     (let ((it p))
       (if it a b)))))

(define *bufsize* (ash 1 17))

(define* (mk-c-atom-dump-fpipe
	  #:key (bufsize *bufsize*)
	  (rsym #t)   (rbv #f)       (rstr #f) (rref #f)
	  (eq-map #f) (equal-map #f) (do-lam #f)
	  (eq-validate #f) (equal-validate #f)
	  (j 0))
  
  (define data (make-hash-table))
  (hashq-set! data 0 (if eq-map
			 eq-map
			 (if (or rsym rref)
			     (make-hash-table)
			     #f)))
  (hashq-set! data 1 (if equal-map
			 equal-map
			 (if (or rbv  rstr) (make-hash-table) #f)))
  (hashq-set! data 2  rstr)
  (hashq-set! data 3  rsym)
  (hashq-set! data 4  rref)
  (hashq-set! data 5  rbv)
  (hashq-set! data 6  rsym)
  (hashq-set! data 7  rref)
  (hashq-set! data 8  rref)
  (hashq-set! data 9  do-lam)
  (hashq-set! data 10 (if eq-validate    eq-validate    (make-hash-table)))
  (hashq-set! data 11 (if equal-validate equal-validate (make-hash-table)))

  (define buf (make-bytevector 100))
  (lambda (scm fpipe)
    (let lp ((bv buf) (i 0) (j 0) (stack (list scm))
	     (first #t))
      (set! buf bv)
      (match (atom-c-serialize bv i j stack data first)
	((p ok bv i j stack)
	 (if p
	     (if (> i bufsize)
		 (begin
		   (fpipe-put-bytevector fpipe bv #:len i)
		   (lp bv 0 j stack #f))
		 (lp bv i j stack #f))
	     (if ok
		 (begin
		   (fpipe-put-bytevector fpipe bv #:len i)
		   (values))
		 (error "error in serialize"))))))))

(define* (mk-c-atom-load-fpipe
	  #:key
	  (bufsize *bufsize*)
	  (map            #f)
	  (map3           #f)
	  (do-lam         #f)
	  (j              0)
	  (j-validate     #f)
	  (equal-validate #f))

  (define data (make-hash-table))
  (hashq-set! data 9 do-lam)
  (hashq-set! data 10 (if j-validate j-validate (make-hash-table)))
  
  (define N     bufsize)
  (define buf   (make-bytevector N))
  (define k     200)
  (lambda (fpipe)
    (let lp ((iii 0) (j 0) (stack '()) (map map) (flags 0) (first 1))
      (aif (x i) (fpipe-get-bytevector fpipe buf #:j iii)
	   ;; final session
	   (let lp ((iii 0) (j j) (stack stack) (map map) (flags flags))
	     (match (atom-c-deserialize buf iii j i stack map flags data
					(logior first 2))
	       ((p ok ii j stack map flags)
		(if p
		    (if (= i ii)
			(error "Could not deserialize stream 1")
			(lp ii j stack map flags))
		    (if ok
			(values x stack)
			(error "Could not deserialize stream 2"))))))

	   (let lp2 ((iii 0) (j j) (stack stack) (map map) (flags flags)
		     (first first))
	     
	     (match (atom-c-deserialize buf iii j (- i k) stack map flags
					data first)
	       ((p ok ii j stack map flags)
		(if p
		    (if (>= ii (- i k))
			(begin
			  (bytevector-copy! buf ii buf 0 (- i ii))
			  (lp  (- i ii)  j stack map flags 0))
			(lp2 ii j stack map flags 0))
		    
		    (if ok
			(error "could not deserialize stream 3")
			(error "could not deserialize stream 4"))))))))))

	   


(define (fpipe->c-atom . l)
  (define fpipe->c-atom
    ((mk-fpipe->scm (apply mk-c-atom-load-fpipe l))))
       
  fpipe->c-atom)

(define (c-atom->fpipe . l)
  (define c-atom->fpipe
    ((mk-scm->fpipe (apply mk-c-atom-dump-fpipe l))))

  c-atom->fpipe)
	

;; ========================== TEST TOOL ===========================
(define (test-c-atom-fpipe)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2)
       (fpipe-construct
	(c-atom->fpipe)
	(fpipe->c-atom)))
     
     (define scm-schemer  (fpipe-schemer ch1 ch2))
     (define v (make-bytevector 10000 1))
     (pk 'scm-schemer-2  (scm-schemer (iota 10)))
     (pk 'scm-schemer-1  (scm-schemer  '((1 . 2) . 3)))
     (pk 'scm-schemer-3  (length (scm-schemer  (iota 10000000))))
     (values))))
