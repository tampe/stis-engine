;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine tools http)
  #:use-module (fibers stis-engine fpipes)
  #:use-module (web server)
  #:use-module (web client)
  
  #:export (fpipe->http-client
	    fpipe-http-server-handler))

;; Enable fpipe stream support for http
(define (fpipe->http-get uri)
  (define (call bv)
    (call-with-values
	(lambda ()
	  (http-get uri #:body bv #:streaming? #t))
      (lambda (a b) b)))
  
  (define run
    (case-lambda
     (()
      (run (make-fpipe)))
     ((p)
      (values p
	      (fpipe-construct-0
	       p
	       fpipe->bv
	       #:tr call
	       p->fpipe)))))
  
  run)

(define (fpipe-http-server-handler p1 p2)
  (define-values (ch1  p1*)  (bv->fpipe (make-channel) p1))
  (define-values (p2*  ch2 ) (fpipe->bv p2 (make-channel)))

  (define schemer (fpipe-scmemer ch1 ch2))
     
  (lambda (request body)
    (values
     '((content-type . (application/octet-stream)))
     (schemer body))))

