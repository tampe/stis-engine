;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine tools atom)
  #:use-module (data stis-data atom)
  #:use-module (rnrs bytevectors)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers stis-engine fpipes)

  #:export (;; low level
	    read-atom-fpipe
	    write-atom-fpipe
	    write-tag-fpipe
	    read-tag-fpipe
	    map-fpipe
	    
	    ;; high level
	    atom->fpipe
	    fpipe->atom
	    mk-scm-fpipe
	    mk-scm->fpipe
	    mk-fpipe->scm
	    
	    ;;test tool
	    test-atom-fpipe))

;; ========================== TOOLBOX ========================
(define* (mk-fpipe->scm atom-load-fpipe)
  (let ()
    (define (fpipe->atom . args)
      (generate-node
       (lambda (p1 p2 t1 t2)
	 (lambda ()
	   (define-values (e cmplx) (fpipe-handle-control))

	   (let lp ()
	     (let ((x (fpipe-peek-u8 p1)))
	       (define (tr)
		 (put-message p2 x)
		 (lp))
	      
	       (if (fpipe-control? x)
		   (cmplx p2
			  x
			  lp
			  (lambda () (values))
			  tr
			  tr)
		     
		   (if (e)
		       (begin
			 (fpipe-get-u8 p1)
			 (lp))
			  
		       (catch #t
			 (lambda ()
			   (define-values (ee x)
			     (apply atom-load-fpipe p1 args))
			       
			   (put-message p2 x)
			      
			   (when (not (eq? ee %fpipe-eof%))
			     (put-message p2 ee))
			       
			   (lp))
		  
			 (lambda x
			   (e #t)
			   (put-message p2 (fpipe-error
					    "atom-load"
					    (format #f "~a" x)))
			   (lp))
			 
			 (lambda x
			   (backtrace)))))))))
       #:name      'fpipe->atomx
       #:t1        #:fpipe
       #:t2        #:scm))
    fpipe->atom))

(define* (mk-scm->fpipe atom-dump-fpipe)
  (let ()
    (define (atom->fpipe . args)
      (generate-node
       (lambda (p1 p2 t1 t2)
	 (lambda ()
	   (define-values (e cmplx) (fpipe-handle-control))
	
	   (let lp ()
	     (define (action x)
	       (catch #t
		(lambda ()
		  (apply atom-dump-fpipe x p2 args)
		  (fpipe-put-u8 p2 %fpipe-eof%))
		(lambda x
		  (e #t)
		  (fpipe-put-u8 p2
				(fpipe-error
				 "atom-load"
				 (format #f "~a" x))))
		(lambda x
		  (backtrace))))
	      	    
	     (let ((x (get-message p1)))
	       (if (fpipe-control? x)
		   (cmplx p2
			  x
			  lp
			  (lambda ()
			    (values))
			  (lambda ()
			    (fpipe-put-u8 p2 x)
			    (lp))
			  (lambda ()
			    (fpipe-put-u8 p2 x)
			    (lp)))
		  
		   (if (e)
		       (lp)
		      
		       (begin
			 (action x)
			 (lp))))))))
       #:name      'atomx->fpipe
       #:t1        #:scm
       #:t2        #:fpipe))
    atom->fpipe))

(define (mk-scm-fpipe scm-> scm<-)
  (values (mk-scm->fpipe scm->)
	  (mk-fpipe->scm scm<-)))

;; =============================== THE PIPES ===========================
(define (atom-fpipe-get-u8 p)
  (let ((x (fpipe-get-u8 p)))
    (if (fpipe-control? x)
	(error (format #f "~a~a got control:~a"
		       "The SCM->FPIPE tool cant handle controls, "
		       "make sure to wrap it."
		       x))
	x)))
	       
(define-values
    (read-atom-fpipe
     write-atom-fpipe
     write-tag-fpipe
     read-tag-fpipe
     write-integer-fpipe

     map-fpipe

     atom-dump-fpipe
     atom-load-fpipe)
  
  (mk-streamer atom-fpipe-get-u8 fpipe-put-u8))

(define-values (atom->fpipe fpipe->atom)
  (mk-scm-fpipe atom-dump-fpipe atom-load-fpipe))

;; ========================== TEST TOOL ===========================
(define (test-atom-fpipe)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2) (fpipe-construct (atom->fpipe)
					       #:id #:id #:id
					       (fpipe->atom)))
          
     (define scm-schemer  (fpipe-schemer ch1 ch2))

     (pk 'scm-schemer-1  (scm-schemer  '((a b) 2 3)))
     (pk 'scm-schemer-2  (length (scm-schemer (iota 1000000)))))))

  
