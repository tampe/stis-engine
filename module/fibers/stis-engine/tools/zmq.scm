;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine tools zmq)
  #:use-module (fibers)
  #:use-module (fibers timers)
  #:use-module (fibers channels)
  #:use-module (fibers stis-engine fpipes)
  #:use-module (fibers stis-engine zmq zmq)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 threads)
  #:use-module (oop goops)
  #:export (fpipe->zmq
	    zmq->fpipe
	    
	    test-zmq-fpipe))

(define-syntax aif
  (syntax-rules ()
    ((_ (it . l) p a b)
     (call-with-values (lambda () p)
       (lambda (it . l)
	 (if it a b))))
    
    ((_ it p a b)
     (let ((it p))
       (if it a b)))))

(define endian  'litle)
(define bufsize  (ash 1 17))

(define (fpipe->zmq socket)
  (generate-node
   (lambda (p1 p2 t1 t2)
     (lambda ()
       (define-values (e cmplx) (fpipe-handle-control))
       (define buf    (make-bytevector bufsize))

       (let lp ()
	 (define (flush j final)
	   (lambda ()	      
	     (define (error it)
	       (e #t)
	       (put-message p2
			    (fpipe-error
			     "zmq-send"
			     (format #f "Error sending, code: ~a" it)))
	       (lp))
	     (let lp2 ((iii 1))
	       (aif (q it) (zmq-send socket
				     buf
				     j
				     (logior ZMQ_DONTWAIT
					     (if final
						 0
						 ZMQ_SNDMORE)))
		    (begin
		      (when final
			(put-message p2 %fpipe-eof%))

		      (lp))

		     
		    (if (or (= it EBUSY) (= it EAGAIN))
			(begin
			  (if (= (modulo iii 10) 0)
			      (sleep 0.01)
			      (sleep 0.0001))
			  (lp2 (+ iii 1)))
			(error it))))))


	 (define (reset)
	   (values))
	  
	 (aif (x j) (fpipe-get-bytevector p1
					  buf
					  #:len bufsize)
	      (cmplx p2
		     x
		     lp
		     reset
		     (flush j #f)
		     (flush j #t))
	      ((flush j #f))))))

   #:t1    #:fpipe
   #:t2    #:scm
   #:name  'fpipe->zmq))

(define (zmq->fpipe socket)
  (generate-node
   (lambda (p1 p2 t1 t2)
    (lambda ()
      (define-values (e cmplx) (fpipe-handle-control))
      (define buf (make-bytevector bufsize))

      (let lp ()     
	(define (lp2 iii)
	  (define (push lp n final?)
	    (fpipe-put-bytevector p2 buf #:len n)
	      
	    (when final?
	      (fpipe-put-u8 p2 %fpipe-eof%))
	      
	    (lp))

	  (define (error it)
	    (e #t)
	    (fpipe-put-u8 p2 (fpipe-error
			      "zmq-recv"
			      (format #f "could not recieve: code ~a" it)))
	    (lp))

	  
	  (aif (p it) (zmq-recv socket
				buf
				ZMQ_DONTWAIT)

	       (if (zmq-recv-more? socket)
		   (push (lambda () (lp2 1)) it #f)
		   (push lp                          it #t))

	       (if (or (= it EBUSY) (= it EAGAIN) (= it 0))
		   (begin
		     (if (= (modulo iii 10) 0)
			 (sleep 0.01)
			 (sleep 0.0001))
		     (lp2 (+ iii 1)))
		   (error it))))
	

	(define (reset) (values))
	  
	(define (flush x)
	  (lambda ()
	    (fpipe-put-u8 p2 x)))

	(let ((x (get-message p1)))
	  (if (fpipe-control? x)
	      (cmplx p2
		     x
		     lp
		     reset
		     (flush x)
		     (lambda () (lp2 1)))
	      (begin
		(e #t)
		(put-message
		 p2
		 (fpipe-error "zmq-revc"
			      "Only controls input alowed"))
		(lp)))))))
   #:t1   #:scm
   #:t2   #:fpipe
   #:name 'zmq->fpipe))


;; ====================== TEST TOOL ======================


#;
(define (test-zmq-fpipe)
  (run-fibers
   (lambda ()
     (define context (zmq-init))
     
     (define socket-client (zmq-socket context ZMQ_REQ))     
     (define socket-server (zmq-socket context ZMQ_REP))

     (zmq-bind    socket-server "tcp://*:5555")
     ;;(zmq-bind socket-server "inproc://a")
     
     (zmq-connect socket-client "tcp://localhost:5555")
     ;;(zmq-connect socket-client "inproc://a")
     (define-values (ch1 ch2)
       (fpipe-construct scm->fpipe
			(fpipe->zmq socket-client)
			(zmq->fpipe socket-server)
                        #:pace
			(fpipe->zmq socket-server)
			(zmq->fpipe socket-client)
			fpipe->scm))
     
     (define schemer (fpipe-schemer ch1 ch2))
     (pk (schemer '(alla balla)))
     (pk (length (schemer (iota 1000000)))))))


(define (test-zmq-fpipe)
  (define context (zmq-init))
  
  (call-with-new-thread
   (lambda ()
     (run-fibers
      (lambda ()
	(define socket-server (zmq-socket context ZMQ_REP))

	;;(zmq-bind socket-server "tcp://*:5555")
	(zmq-bind socket-server "inproc://a")
	
	(define-values (ch1 ch2)
	  (fpipe-construct (zmq->fpipe socket-server)
			   (fpipe->list)
			   (list->fpipe)
			   (fpipe->zmq socket-server)))

	(define schemer (fpipe-schemer ch1 ch2))
	(let lp ()
	  (pk 'server-returns (schemer %fpipe-eof%))
	  (lp))))))

     
  (run-fibers
   (lambda ()
     (define socket-client (zmq-socket context ZMQ_REQ))     
     
     ;;(zmq-connect socket-client "tcp://127.0.0.1:5555")
     (zmq-connect socket-client "inproc://a")

     
     (define-values (ch1 ch2)
       (fpipe-construct (scm->fpipe)
			(fpipe->zmq socket-client)
			(zmq->fpipe socket-client)
			(fpipe->scm)))
     
     (define schemer (fpipe-schemer ch1 ch2))
     
     (pk 'client-returns-1 (schemer '(alla balla)))
     (pk 'client-returns-2 (schemer '(allaz 12 ballaz)))
     (pk 'client-returns-3 (length (schemer (iota 100000)))))))
