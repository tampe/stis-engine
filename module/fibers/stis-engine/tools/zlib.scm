;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine tools zlib)
  #:use-module (fibers stis-engine zlib zlib)
  #:use-module (fibers)
  #:use-module (rnrs bytevectors)
  #:use-module (fibers stis-engine fpipes)
  #:use-module (fibers stis-engine tools c-atom)

  #:export (compress-from-fpipe-to-fpipe
	    uncompress-from-fpipe-to-fpipe
	    ;; test
	    test-zlib
))

(define zlib-bufsize-in  (ash 1 15))
(define zlib-bufsize-out (ash 1 15))

(define  (mk-compress-from-x-to-x deflate deflate?)
  (define (compress-from-x-to-x get-bv put-u8 put-bv)
    (lambda* (#:key
	      (parallel?    #f)
	      (level        3)
	      (bufsize-in   zlib-bufsize-in)
	      (bufsize-out  zlib-bufsize-out))
	       
      (define (mk-stream stream in out)
	(if deflate?
	    (make-deflate-stream stream level in out)
	    (make-inflate-stream stream in out)))
	
      (generate-node
       (lambda  (p1 p2 t1 t2)
	 ;; The fiber doing work
	 (lambda ()
	   (define bv-in   (make-bytevector bufsize-in))
	   (define bv-out  (make-bytevector bufsize-out))
	   (define-values  (e cmplx) (fpipe-handle-control))

	   ;; Dynamic information
	   (define stream     #f)
	   (define flush      #f)
	   (define N-out      bufsize-out)
	   (define N-in       0)
	   (define reset?     #t)
	   (define (reset)
	     (set! flush   Z_NO_FLUSH)
	     (set! reset?  #t)
	     (set! N-out   bufsize-out)
	     (set-avail-out stream N-out))
	    
	   (define (push)
	     (put-bv p2 bv-out #:len (- bufsize-out N-out)))
	    
	   (define (mk-error a b lp)
	     (reset)
	     (e #t)
	     (put-u8 p2 (fpipe-error a b))
	     (lp))

	   (define (loop-2 lp llp)		
	     (let lp2 ()
	       (set-avail-out stream bufsize-out)
	       (set-next-out stream bv-out)
	      
	       (aif (p R nin nout) (deflate stream flush)
		    (mk-error R nin lp)
		    (begin
		      (set! N-in  nin)
		      (set! N-out nout)
		      (push)
		      (if (= N-out 0)
			  (lp2)
			  (llp R))))))
	  	
		
	   (let lp ()
	     (define (continue-lp kind)
	       (lambda (R)
		 (cond
		  ((if deflate?
		       (= flush Z_FINISH)
		       (= R Z_STREAM_END))
		  
		   (begin
		     (put-u8 p2 %fpipe-eof%)
		     (reset)
		     (lp)))
		  
		  ((= kind 1)
		   (put-u8 p2 %fpipe-flush%))
		 
		  (else
		   (lp)))))
      
	     (define (goto-lp2 j kind)
	       (lambda ()
		 (set! N-in j)
		 (set-avail-in stream j)
		 (set-next-in stream bv-in)
		 (set! flush
		       (if deflate?
			   (if (= kind 2) Z_FINISH Z_NO_FLUSH)
			   Z_NO_FLUSH))
		
		 (loop-2 lp (continue-lp kind))))
	    	
	     (define (read)
	       (aif (x j) (get-bv p1 bv-in)
		    ;;(    out x lp reset flush      eof)
		    (cmplx p2  x lp reset (goto-lp2 j 1) (goto-lp2 j 2))
		   
		    (if (e)
			(lp)
			((goto-lp2 j 0)))))

	     (if (or (let ((r reset?)) (set! reset? #f) r)
		     (not stream))
		 (aif (p a b) (mk-stream stream bv-in bv-out)
		      (mk-error a b lp)
		      (begin
			(set! stream a)
			(read)))
		 (read)))))
       #:parallel? parallel?
       #:t1        #:fpipe
       #:t2        #:fpipe
       #:name      'zlibber)))
  compress-from-x-to-x)
		

;;; ====================== UNFOLD TO TOOLBOX ===================
(define compress-from-x-to-x
  (mk-compress-from-x-to-x deflate #t))

(define uncompress-from-x-to-x
  (mk-compress-from-x-to-x inflate #f))

(define compress-from-fpipe-to-fpipe
  (compress-from-x-to-x fpipe-get-bytevector
			fpipe-put-u8 fpipe-put-bytevector))

(define uncompress-from-fpipe-to-fpipe
  (uncompress-from-x-to-x fpipe-get-bytevector
			  fpipe-put-u8 fpipe-put-bytevector))


;; ======================= TEST TOOL ===========================
(define (test-zlib)
  (run-fibers
   (lambda ()
     (define-values (ch1 ch2)
       (fpipe-construct (c-atom->fpipe)
			(compress-from-fpipe-to-fpipe #:level 3)
			(uncompress-from-fpipe-to-fpipe)
			(fpipe->c-atom)))

     (define-values (ch3 ch4)
       (fpipe-construct (c-atom->fpipe)
			(fpipe-count)))

     (define-values (ch5 ch6)
       (fpipe-construct (c-atom->fpipe)
			(compress-from-fpipe-to-fpipe #:level 3)
			(fpipe-count)))
     
     (define compress-schemer1 (fpipe-schemer ch1 ch2))
     (define t1                (fpipe-schemer ch3 ch4))
     (define t2                (fpipe-schemer ch5 ch6))
     ;;(define v                 (iota 1000000))
     (define v                 (make-bytevector 100000000 0))
     
     (pk 'compress/uncompress-1 (compress-schemer1 '(1 2 3 4)))
     
     (pk 'len (bytevector-length (compress-schemer1 v)))

     (pk 'len-uncompressed (t1 v))
     (pk 'len-compressed   (t2 v))))) 
