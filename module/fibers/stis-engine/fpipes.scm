;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (fibers stis-engine fpipes)
  #:use-module (oop goops)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers stis-engine fpipes core)
  #:use-module (fibers stis-engine fpipes util)
  #:use-module (fibers stis-engine fpipes functional)
  #:use-module (fibers stis-engine fpipes cond)
  #:use-module (fibers stis-engine fpipes join)
  #:use-module (fibers stis-engine fpipes producers)
  #:use-module (fibers stis-engine fpipes consumers)
  #:use-module (fibers stis-engine fpipes macro)  
  #:use-module (rnrs bytevectors)

  #:re-export-and-replace (bytevector-copy!)
  
  #:re-export (;; util macro
	       aif
	       
	       
	       ;; Low level
	       make-fpipe
	       fpipe?
	       
	       fpipe-control?

	       fpipe-eof?
	       
	       fpipe-error
	       fpipe-error?
	       fpipe-error-type
	       fpipe-error-msg

	       %fpipe-flush%
	       %fpipe-end%
	       %fpipe-eof%
	       %fpipe-reset%
	    
	       fpipe-get-u8
	       fpipe-get-bytevector
	       fpipe-peek-u8
	       fpipe-put-u8
	       fpipe-put-bytevector

	       fpipe-handle-control

	       ;; util
	       fpipe-id
	       fpipe-demuplex
	       fpipe-multiplex
	       fpipe-pk	    
	       fpipe-tee
	       fpipe-null
	       fpipe-skip
	       fpipe-prepend
	       fpipe-count
	       
	       ;; functional
	       fpipe-map
	       fpipe-fold
	       fpipe-message-fold
	       
	       ;; producers
	       list->fpipe
	       bv->fpipe
	       string->fpipe
	       scm->fpipe

	       ;; consumers
	       fpipe->list
	       fpipe->bv
	       fpipe->string
	       fpipe->scm

	       ;; Higher level
	       generate-node
	       define-fpipe-construct
	       fpipe-construct
	       fpipe-schemer)
	    	    
	   
  #:export (test-fpipe))
		              	 
;;; ================================== TEST TOOLBOX =========================
(define (test-fpipe)
  (define (L x) (lambda () x))
  (run-fibers
   (lambda ()
     (pk '================================-build-pipes)
     
     (pk 'simple-id)
     (define-values (pp1  pp2) (fpipe-construct (fpipe-id)))
     

     (pk 'list->fpipe->list)
     (define-values (ch1 ch2) (fpipe-construct (list->fpipe)
					       (fpipe-id)
					       (fpipe->list)))

     (pk 'scm->fpipe->scm)
     (define-values (ch3 ch4) (fpipe-construct (scm->fpipe)
					       (fpipe-id)
					       (fpipe->scm)))

     (pk 'fpipe-let)
     (define-values (ch5 ch6) (fpipe-construct (list->fpipe)
					       (let ((it1 it2)
						     (a   it2))
						 (fpipe-id))
					       (fpipe->list)))

     (pk 'scm-let)
     (define-values (ch7 ch8) (fpipe-construct 
			       (let (it
				     (a it))
				 (list->fpipe))				 
			       (fpipe->list)))

     (pk 'scm-cond)
     (define-values (ch9 ch10) (fpipe-construct 
				(let (it)
				  (cond
				   ((pair? it)
				    (fpipe-pk 'yes))
				   (else
				    (fpipe-pk 'no))))))

     (pk 'fpipe-cond)
     (define-values (ch11 ch12) (fpipe-construct
				 (list->fpipe)
				 (let ((it1 it2))
				   (cond
				    ((= it2 2)
				     (fpipe-pk 'yes))
				    (else
				     (fpipe-pk 'no)))
				   (fpipe->list))))

     (pk 'fpipe-if)
     (define-values (ch13 ch14) (fpipe-construct
				 (list->fpipe)
				 (let ((it1 it2))
				   (if (= it2 2)
				       (fpipe-pk 'yes)
				       (fpipe-pk 'no)))
				 (fpipe->list)))
     (pk 'when)
     (define-values (ch15 ch16) (fpipe-construct
				 (list->fpipe)
				 (let ((it1 it2))
				   (when (= it2 2)
				     (fpipe-pk 'yes)))
				 (fpipe->list)))

     (pk 'skip)
     (define-values (ch17 ch18) (fpipe-construct
				 (list->fpipe)
				 (fpipe-skip 2)
				 (fpipe->list)))

     (pk 'prepend)
     (define-values (ch19 ch20) (fpipe-construct
				 (list->fpipe)
				 (fpipe-prepend 10 11 12)
				 (fpipe->list)))

     (pk '===========================-make-schemers)
     (define list-schemer (fpipe-schemer ch1  ch2 ))
     (define scm-schemer  (fpipe-schemer ch3  ch4 ))
     (define let-fpipe    (fpipe-schemer ch5  ch6 ))
     (define let-scm      (fpipe-schemer ch7  ch8 ))
     (define cond-scm     (fpipe-schemer ch9  ch10))
     (define cond-fpipe   (fpipe-schemer ch11 ch12))
     (define if-fpipe     (fpipe-schemer ch13 ch14))
     (define when-fpipe   (fpipe-schemer ch15 ch16))
     (define fpipe-skiper (fpipe-schemer ch17 ch18))
     (define prepend      (fpipe-schemer ch19 ch20))
     
     (pk '==========================-executing-schemers)
     (pk 'list-schemer (list-schemer '(1 2 3)))
     (pk 'scm-schemer  (scm-schemer  '((1 a) b c)))
     (pk 'let-fpipe    (let-fpipe    '(1 2 3)))
     (pk 'let-scm      (let-scm      '(1 2 3)))
     (pk 'cond-scm-1   (cond-scm     '(1 2 3)))
     (pk 'cond-scm-0   (cond-scm     128))
     (pk 'cond-fpipe-1 (cond-fpipe   '(1 2 3)))
     (pk 'cond-fpipe-0 (cond-fpipe   '(1 3 4)))
     (pk 'if-fpipe-1   (if-fpipe     '(1 2 3)))
     (pk 'if-fpipe-0   (if-fpipe     '(1 3 4)))
     (pk 'when-fpipe-1 (when-fpipe   '(1 2 3)))
     (pk 'when-fpipe-0 (when-fpipe   '(1 3 4)))
     (pk 'skip         (fpipe-skiper '(1 3 4 6 4 3)))
     (pk 'prepend      (prepend      '(4 6 4 3)))
     (values))))
     
